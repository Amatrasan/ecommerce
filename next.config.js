const { nextI18NextRewrites } = require('next-i18next/rewrites')
const withImages = require('next-images')


const localeSubpaths = {}

module.exports = {
  rewrites: async () => nextI18NextRewrites(localeSubpaths),
  publicRuntimeConfig: {
    localeSubpaths,
  }
}



module.exports = withImages()
