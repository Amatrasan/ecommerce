import style from './Footer.module.scss'
import GridWrapper from '../UI/GridWrapper'
import { Facebook, Instagram } from '../UI/Icons'
import classnames from '../../utils/classnames'
import { useRouter } from 'next/router'

const MAIL = 'development@webpunk.site'
const SUBJECT = 'New message for WebPunk Team'

const Footer = () => {
  const router = useRouter()

  return (
    <footer className={style.footer}>
      <GridWrapper className={style.footer_wrapper}>
        <div className={classnames(style.footer__footer_container, style.footer_container)}>
          <div className={ classnames(style.footer_container__fc_content, style.fc_content)}>
            <div className={style.fc_content__block}>
              <div className={style.title}>Интернет-магазин</div>
              <div className={style.link}><a href={`mailto:${MAIL}?subject=${SUBJECT.replace(' ', '%20')}`} target={'_blank'} rel={'noreferrer'}>{MAIL}</a></div>
              <div className={style.social_box}>
                <div onClick={() => window.open('https://www.instagram.com/', '_blank')}>
                  <Instagram />
                </div>
                <div onClick={() => window.open('https://www.facebook.com/', '_blank')}>
                  <Facebook />
                </div>
                {/*<div>*/}
                {/*  <Facebook />*/}
                {/*</div>*/}
              </div>
            </div>
            <div className={style.fc_content__block}>
              <div className={style.title}>Общая информация</div>
              <div className={style.label} onClick={() => router.push('/privacy_policy')}>Политика конфиденциальности</div>
              <div className={style.label} onClick={() => router.push('/terms')}>Условия использования</div>
              {/*<div className={style.label}>Item 3</div>*/}
            </div>
            <div className={style.fc_content__block}>
              {/*<div className={style.title}>Title</div>*/}
              {/*<div className={style.label}>Item 1</div>*/}
              {/*<div className={style.label}>Item 2</div>*/}
              {/*<div className={style.label}>Item 3</div>*/}
            </div>
          </div>
        </div>
      </GridWrapper>

    </footer>
  )
}

export default Footer
