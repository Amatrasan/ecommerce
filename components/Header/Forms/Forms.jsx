import style from './Forms.module.scss'
import PopUp from '../../UI/PopUp'
import * as types from '../PopUpTypes'
import Input from '../../UI/Input'
import ButtonPrimary from '../../UI/ButtonPrimary'
import PropTypes from 'prop-types'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import {
  APP_AUTH_LOGIN_FORGOT_REQUEST,
  APP_AUTH_LOGIN_REQUEST,
  APP_AUTH_REGISTER_REQUEST
} from '../../../store/auth/actionTypes'
import { useDispatch, useSelector } from 'react-redux'

const Forms = (props) => {
  const dispatch = useDispatch()
  const auth = useSelector(state => state.auth)

  // useEffect(() => {
  //   if (auth && auth.token) {
  //     props.changePopUp('')
  //   }
  // }, [auth, props])

  const formikLogin = useFormik({
    initialValues: {
      email: '',
      password: ''
    },
    validationSchema: Yup.object({
      email: Yup.string()
        .email('Email введён неверно')
        .trim()
        .required('Введите Email'),
      password: Yup.string()
        .required('Введите номер телефона')
        .test('min', 'Не менее 6 символов', val => val && val.toString().length >= 6),
    }),
    onSubmit: values => {
      dispatch({ type: APP_AUTH_LOGIN_REQUEST, email: values.email, password: values.password })
      closePopUp()
    }
  })

  const formikRegister = useFormik({
    initialValues: {
      email: '',
      password: '',
      firstName: '',
      lastName: '',
      country: '',
      city: '',
      address: ''
    },
    validationSchema: Yup.object({
      email: Yup.string()
        .email('Email введён неверно')
        .trim()
        .required('Введите Email'),
      password: Yup.string()
        .required('Введите номер телефона')
        .test('min', 'Не менее 6 символов', val => val && val.toString().length >= 6),
      firstName: Yup.string().required('Введите имя'),
      lastName: Yup.string().required('Введите фамилию'),
      country: Yup.string().required('Введите страну'),
      city: Yup.string().required('Введите город'),
      address: Yup.string().required('Введите адрес')
    }),
    onSubmit: values => {
      dispatch({ type: APP_AUTH_REGISTER_REQUEST, credentials: values, callback: props.afterRegister })
      closePopUp()
    }
  })

  const formikForgot = useFormik(
    {
      initialValues: { email: '' },
      validationSchema: Yup.object({
        email: Yup.string()
          .email('Email введён неверно')
          .trim()
          .required('Введите Email')
      }),
      onSubmit: values => {
        dispatch({ type: APP_AUTH_LOGIN_FORGOT_REQUEST, email: values.email })
        closePopUp()
      }
    }
  )


  const closePopUp = () => props.changePopUp('')

  return (
    <>
      <PopUp show={props.showPopUp === types.POP_UP_LOGIN} onClose={closePopUp} title={'Вход'}>
        <form onSubmit={formikLogin.handleSubmit} className={style.form}>
          <Input
            name="email"
            type="email"
            onChange={formikLogin.handleChange}
            value={formikLogin.values.email}
            message={formikLogin.errors.email}
            label={'Email'}
          />
          <Input
            name="password"
            type="password"
            autoComplete={'true'}
            onChange={formikLogin.handleChange}
            value={formikLogin.values.password}
            message={formikLogin.errors.password}
            label={'Пароль'}
          />
          <div className={style.error}>{auth ? auth.message : ''}</div>
          <div className={style.btn_box}>
            <ButtonPrimary type={'submit'} className={style.btn}>Войти</ButtonPrimary>
            <div className={style.btn_box__sub_box}>
              {/*<ButtonPrimary*/}
              {/*  type={'button'}*/}
              {/*  className={style.btn}*/}
              {/*  onClick={() => props.changePopUp(types.POP_UP_FORGOT)}*/}
              {/*>*/}
              {/*  Забыл пароль*/}
              {/*</ButtonPrimary>*/}
              <ButtonPrimary
                type={'submit'}
                className={style.btn}
                onClick={() => props.changePopUp(types.POP_UP_REGISTER)}
              >Регистрация</ButtonPrimary>
            </div>
          </div>
        </form>
      </PopUp>
      <PopUp show={props.showPopUp === types.POP_UP_REGISTER} onClose={closePopUp} title={'Регистрация'}>
        <form onSubmit={formikRegister.handleSubmit} className={style.form}>
          <Input
            name="email"
            type="email"
            onChange={formikRegister.handleChange}
            value={formikRegister.values.email}
            message={formikRegister.errors.email}
            label={'Email'}
          />
          <Input
            name="password"
            type="password"
            autoComplete={'true'}
            onChange={formikRegister.handleChange}
            value={formikRegister.values.password}
            message={formikRegister.errors.password}
            label={'Пароль'}
          />
          <Input
            name="firstName"
            type="text"
            onChange={formikRegister.handleChange}
            value={formikRegister.values.firstName}
            message={formikRegister.errors.firstName}
            label={'Имя'}
          />
          <Input
            name="lastName"
            type="text"
            onChange={formikRegister.handleChange}
            value={formikRegister.values.lastName}
            message={formikRegister.errors.lastName}
            label={'Фамилия'}
          />
          <Input
            name="country"
            type="text"
            onChange={formikRegister.handleChange}
            value={formikRegister.values.country}
            message={formikRegister.errors.country}
            label={'Страна'}
          />
          <Input
            name="city"
            type="text"
            onChange={formikRegister.handleChange}
            value={formikRegister.values.city}
            message={formikRegister.errors.city}
            label={'Город'}
          />
          <Input
            name="address"
            type="text"
            onChange={formikRegister.handleChange}
            value={formikRegister.values.address}
            message={formikRegister.errors.address}
            label={'Адрес'}
          />
          <div className={style.error}>{auth ? auth.message : ''}</div>
          <div className={style.btn_box}>
            {props.quickBuy ? (
              <ButtonPrimary type={'submit'} className={style.btn}>Сохранить и купить</ButtonPrimary>
            ) : (
              <>
                <ButtonPrimary type={'submit'} className={style.btn}>Зарегистрироваться</ButtonPrimary>
                <div className={style.btn_box__sub_box}>
                  <ButtonPrimary
                    type={'button'}
                    className={style.btn}
                    onClick={() => props.changePopUp(types.POP_UP_LOGIN)}
                  >
                    Войти
                  </ButtonPrimary>
                </div>
              </>
            )}

          </div>
        </form>
      </PopUp>
      <PopUp show={props.showPopUp === types.POP_UP_FORGOT} onClose={closePopUp} title={'Восстановление пароля'}>
        <form onSubmit={formikForgot.handleSubmit} className={style.form}>
          <Input
            name="email"
            type="email"
            onChange={formikForgot.handleChange}
            value={formikForgot.values.email}
            message={formikForgot.errors.email}
          />

          <div className={style.btn_box}>
            <ButtonPrimary type={'submit'} className={style.btn}>Отправить</ButtonPrimary>
            <div className={style.btn_box__sub_box}>
              <ButtonPrimary
                type={'button'}
                onClick={() => props.changePopUp(types.POP_UP_LOGIN)}
                className={style.btn}
              >
                Войти
              </ButtonPrimary>
            </div>
          </div>
        </form>
      </PopUp>
    </>
  )

}

Forms.propTypes = {
  showPopUp: PropTypes.string,
  changePopUp: PropTypes.func.isRequired,
  afterRegister: PropTypes.func
}

export default Forms
