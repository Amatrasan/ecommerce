import style from './RightCorner.module.scss'
import PropTypes from 'prop-types'
import classnames from '../../../utils/classnames'
import ButtonPrimary from '../../UI/ButtonPrimary'
import { Basket, Human, User } from '../../UI/Icons'
import { useState } from 'react'
import * as types from '../PopUpTypes'
import Forms from '../Forms'
import { useDispatch, useSelector } from 'react-redux'
import { useRouter } from 'next/router'
import { APP_AUTH_LOGOUT } from '../../../store/auth/actionTypes'
import PopUp from '../../UI/PopUp'

const RightCorner = (props) => {
  const isAuth = useSelector(state => state.auth?.token)
  const router = useRouter()
  const dispatch = useDispatch()
  const mainBasket = useSelector(state => state.app.baskets.find(basket => basket.main))
  const [showPopUp, setShowPopUp] = useState('')

  const changePopUp = (type) => setShowPopUp(type)

  // const showFindBox = () => {
  //   const inputBox = document.querySelector(`.${style.find_box__input}`)
  //   inputBox.style.display = 'block'
  //   inputBox.style.opacity = 1
  //   inputBox.style.width = '100px'
  //   setTimeout(() => inputBox.querySelector('input').focus(), 0)
  //   inputBox.querySelector('input').addEventListener('blur', hideFindBox)
  // }
  //
  // const hideFindBox = (e) => {
  //   const inputBox = e.target.parentNode
  //   inputBox.style.opacity = 0
  //   inputBox.style.width = 0
  //   inputBox.style.display = ''
  // }

  const handleUserClick = () => {
    if (!isAuth) {
      setShowPopUp(types.POP_UP_LOGIN)
    } else {

    }
  }

  const goToBasket = () => {
    if (!isAuth) {
      setShowPopUp(types.POP_UP_LOGIN)
    } else {
      router.push('/cabinet#basket')
    }
  }

  // const getAdminBox = () => (
  //   <>
  //     <ButtonPrimary onClick={handleUserClick} className={style.button}>
  //       {isAuth ? <Human /> : <User /> }
  //     </ButtonPrimary>
  //     {isAuth && <div className={style.context_menu}>
  //       <ButtonPrimary onClick={() => router.push('/cabinet')}>Мой кабинет</ButtonPrimary>
  //       <ButtonPrimary onClick={logout}>Выход</ButtonPrimary>
  //     </div>}
  //   </>
  // )
  //
  // const getUserBox = () => (
  //   <>
  //     <ButtonPrimary onClick={handleUserClick} className={style.button}>
  //       {isAuth ? <Human /> : <User /> }
  //     </ButtonPrimary>
  //     {isAuth && <div className={style.context_menu}>
  //       <ButtonPrimary onClick={() => router.push('/cabinet')}>Мой кабинет</ButtonPrimary>
  //       <ButtonPrimary onClick={logout}>Выход</ButtonPrimary>
  //     </div>}
  //   </>
  // )

  const logout = () => dispatch({ type: APP_AUTH_LOGOUT })

  return (
    <div className={classnames(style.header_head__right_corner, style.right_corner)}>
      {!props.admin && (
        <ButtonPrimary className={style.button} onClick={() => setShowPopUp(types.POP_UP_BASKET)}>
          <Basket />
        </ButtonPrimary>)
      }
      <div className={style.user_box}>
        <ButtonPrimary onClick={handleUserClick} className={style.button}>
          {isAuth ? <Human /> : <User /> }
        </ButtonPrimary>
        {isAuth && <div className={style.context_menu}>
          {!props.admin && <ButtonPrimary className={style.btn} onClick={() => router.push('/cabinet')}>Мой кабинет</ButtonPrimary>}
          <ButtonPrimary className={style.btn} onClick={logout}>Выход</ButtonPrimary>
        </div>}
      </div>
      <PopUp show={showPopUp === types.POP_UP_BASKET} onClose={() => changePopUp('')} title={'Корзина'}>
        <div className={style.pop_up_basket}>
          <div className={style.title}>
            В вашей корзине {(mainBasket.products && mainBasket.products.length) || 0} товаров
          </div>
          <div className={style.btn_box}>
            <ButtonPrimary className={style.btn} onClick={goToBasket}>Перейти в корзину</ButtonPrimary>
          </div>
        </div>
      </PopUp>
      <Forms changePopUp={changePopUp} showPopUp={showPopUp} />
    </div>
  )
}

RightCorner.propTypes = {
  admin: PropTypes.bool
}

export default RightCorner
