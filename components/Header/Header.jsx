import React, { useEffect, useState } from 'react'
import style from './Header.module.scss'
import { withTranslation } from '../../i18n'
import NeoWrapper from '../UI/NeoWrapper'
import { Shoes, Shirt, Backpack } from '../UI/Icons'
import PropTypes from 'prop-types'
import { useRouter } from 'next/router'
import { useDispatch, useSelector } from 'react-redux'
import GridWrapper from '../UI/GridWrapper'
import LeftCorner from './LeftCorner'
import RightCorner from './RightCorner'
import { APP_USER_GET } from '../../store/user/actionTypes'
import api from './../../services/ApiService'

const Header = ({ t, showOptions = true, admin = false }) => {
  const [categories, setCategories] = useState([])
  const router = useRouter()
  const dispatch = useDispatch()
  const isAuth = useSelector(state => state.auth?.token)

  useEffect(() => {
    if (isAuth) {
      dispatch({ type: APP_USER_GET })
    }
  }, [dispatch, isAuth])

  const linkTo = (url) => {
    router.push('/catalog/' + url)
  }

  useEffect(() => {
    const getCat = async () => {
      const res = await api.getCategories()
      return res.data
    }
    getCat().then(res => setCategories(res))
  },[])

  return (
    <GridWrapper>
      <header className={style.header}>
        <div className={[style.header__header_head, style.header_head].join(' ')}>
          <LeftCorner />
          <div className={style.header__logo} onClick={() => router.push('/')}>WebPunk</div>
          <RightCorner admin={admin} />
        </div>
        {(showOptions || !admin) && (
          <div className={[style.header__header_footer, style.header_footer].join(' ')}>
            {!!categories.length && categories.map(cat => (
              <NeoWrapper className={style.hf__label} onClick={() => linkTo(cat.id)} key={cat.id}>
                <div>
                  {/*<Shoes />*/}
                  <span>{cat.label}</span>
                </div>
              </NeoWrapper>
            ))}

            {/*<NeoWrapper className={style.hf__label} onClick={() => linkTo('shirts')}>*/}
            {/*  <div>*/}
            {/*    /!*<Shirt />*!/*/}
            {/*    <span>{t('navMenu.items.shirts')}</span>*/}
            {/*  </div>*/}
            {/*</NeoWrapper>*/}
            {/*<NeoWrapper className={style.hf__label} onClick={() => linkTo('accessories')}>*/}
            {/*  <div>*/}
            {/*    /!*<Backpack />*!/*/}
            {/*    <span>{t('navMenu.items.accessories')}</span>*/}
            {/*  </div>*/}
            {/*</NeoWrapper>*/}
          </div>)}


      </header>
    </GridWrapper>
  )
}

Header.propTypes = {
  t: PropTypes.func.isRequired,
  showOptions: PropTypes.bool,
  admin: PropTypes.bool
}

export default withTranslation('common')(Header)
