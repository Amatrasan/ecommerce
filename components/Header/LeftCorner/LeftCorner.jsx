import style from './LeftCorner.module.scss'
import ButtonPrimary from '../../UI/ButtonPrimary'
import { Location } from '../../UI/Icons'
import React from 'react'

const LeftCorner = () => {

  const getLocation = () => {
    navigator.geolocation.getCurrentPosition((pos) => console.log('success', pos), (err) => console.log('err', err))
  }

  return (
    <div className={style.header_head__left_corner}>
      <ButtonPrimary onClick={getLocation}>
        <Location  />
      </ButtonPrimary>
      {/*<ButtonPrimary>*/}
      {/*  <Earth />*/}
      {/*</ButtonPrimary>*/}
      {/*<ButtonPrimary>*/}
      {/*  <Earth />*/}
      {/*</ButtonPrimary>*/}
    </div>
  )
}

export default LeftCorner
