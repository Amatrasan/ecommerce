import { useEffect } from 'react'
import { createPortal } from 'react-dom'
import PropTypes from 'prop-types'
import style from './PopUp.module.scss'
import classnames from '../../../utils/classnames'
import ButtonPrimary from '../ButtonPrimary'

const PopUp = (props) => {

  useEffect(() => {
    const hidePopUp = (e) => {
      if (e.keyCode === 27) {
        props.onClose(false)
      }
    }

    window.addEventListener('keydown', hidePopUp)
    return () => window.removeEventListener('keydown', hidePopUp)
  }, [props])

  if (props.show) {
    return (
      createPortal(
        <div className={style.pop_up__overlay}>
          <div className={classnames(style.pop_up__content, props.className)}>
            <ButtonPrimary className={style.close} onClick={props.onClose}>X</ButtonPrimary>
            {props.title && <div className={style.title}>{props.title}</div>}
            {props.children}
          </div>
        </div>, document.body)
    )
  } else {
    return null
  }
}

PopUp.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
  show: PropTypes.bool,
  onClose: PropTypes.func
}

export default PopUp
