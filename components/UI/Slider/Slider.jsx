import React, { useEffect, useRef, forwardRef } from 'react'
import SliderSlick from 'react-slick'
import PropTypes from 'prop-types'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import style from './Slider.module.scss'
import classnames from '../../../utils/classnames'

const Slider = forwardRef(({ children, className, ...props }, ref) => {
  const refSlider = useRef()

  useEffect(() => {
    const onWheel = (e) => {
      e.preventDefault()
      if (Math.sign(e.deltaY) < 0) {
        refSlider.current.slickNext()
      } else {
        refSlider.current.slickPrev()
      }
    }
    if (refSlider.current) {
      refSlider.current.innerSlider.list.addEventListener('wheel', onWheel, { passive: false })
    }
  }, [ref])

  useEffect(() => {

  }, [ref])

  const gerRef = (sliderRef) => {
    if (ref) {
      ref.current = sliderRef
    }
    refSlider.current = sliderRef
  }

  return (
    <SliderSlick {...props} ref={gerRef} className={classnames(style.slider, className)}>
      {children}
    </SliderSlick>
  )
})

Slider.displayName = 'Slider'

Slider.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node
}

export default Slider
