import { useState, useEffect } from 'react'
import style from './Range.module.scss'
import PropTypes from 'prop-types'
import InputRange from 'react-input-range'
import 'react-input-range/lib/css/index.css'

const Range = (props) => {
  const { min = 0, max = 10, onChange, onChangeComplete, formatLabel } = props
  const [state, setState] = useState({ min: 1, max: 2 })

  useEffect(() => {
    setState({ min: min + 1, max: max - 1 })
  }, [max, min])

  const handleChange = (value) => {
    if (onChange) {
      onChange(value)
    }
    setState(value)
  }

  const handleChangeComplete = (val) => {
    if (onChangeComplete) {
      onChangeComplete(val)
    }
  }

  const customFormatLabel = (side) => {
    if (formatLabel) {
      return formatLabel(state[side], side)
    } else {
      return state[side]
    }
  }

  const formatLabelDisabled = () => null

  return (
    <div className={style.range_box}>
      <div className={style.range_box__labels}>
        <span>{customFormatLabel('min')}</span>
        <span>{customFormatLabel('max')}</span>
      </div>
      <InputRange
        maxValue={max}
        minValue={min}
        value={state}
        formatLabel={formatLabelDisabled}
        onChange={handleChange}
        onChangeComplete={handleChangeComplete}
        allowSameValues={true}
      />
    </div>
  )
}

Range.propTypes = {
  min: PropTypes.number.isRequired,
  max: PropTypes.number.isRequired,
  onChangeComplete: PropTypes.func,
  onChange: PropTypes.func,
  formatLabel: PropTypes.func
}

export default Range
