import PropTypes from 'prop-types'
import style from './ButtonPrimary.module.scss'
import classnames from '../../../utils/classnames'

const ButtonPrimary = ({ children, className, ...props }) => {
  return (
    <button { ...props } className={classnames(style.button_primary, className)}>
      {children}
    </button>
  )
}

ButtonPrimary.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node.isRequired
}

export default ButtonPrimary

