import { memo } from 'react'
import style from './ButtonColor.module.scss'
import PropTypes from 'prop-types'
import ButtonPrimary from '../ButtonPrimary'
import classnames from '../../../utils/classnames'

const ButtonColor = ({ color, onClick = null, className, ...rest }) => (
  <ButtonPrimary
    className={classnames(style.color_btn, className)}
    onClick={onClick}
    { ...rest }
  >
    <div style={{ background: `linear-gradient(90deg, ${color}, transparent)` }} />
  </ButtonPrimary>
)

ButtonColor.propTypes = {
  onClick: PropTypes.func,
  color: PropTypes.string.isRequired,
  className: PropTypes.string
}

export default memo(ButtonColor)
