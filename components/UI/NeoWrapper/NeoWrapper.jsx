import PropTypes from 'prop-types'
import style from './NeoWrapper.module.scss'
import classnames from '../../../utils/classnames'

const NeoWrapper = ({ className, children, ...rest }) => {
  return (
    <div className={classnames(style.neo_wrapper, className)} { ...rest }>
      {children}
    </div>
  )
}

NeoWrapper.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node.isRequired
}

export default NeoWrapper

