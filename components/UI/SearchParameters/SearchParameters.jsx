import { memo, useState, useEffect } from 'react'
import style from './SearchParameters.module.scss'
import PropTypes from 'prop-types'
import Price from './Price'
import Select from '../Select'

// сортировка
// ценовой диапазон
// размеры
// цвета

const translate = [
  {
    text: 'Белый',
    value: 'white'
  },
  {
    text: 'Синий',
    value: 'blue',
  },
  {
    text: 'Зеленый',
    value: 'green'
  },
  {
    text: 'Красный',
    value: 'red'
  },
  {
    text: 'Желтый',
    value: 'yellow'
  },
  {
    text: 'Аквамарин',
    value: 'aqua'
  },
  {
    text: 'Черный',
    value: 'black'
  }
]

const SearchParameters = (props) => {
  const [SP, setSP] = useState({})

  useEffect(() => {
    props.onSearchChange(SP)
  }, [SP, props])

  const sortConfig = {
    title: 'Сортировка',
    options: [
      {
        value: 'Цена по убыванию',
        code: '1'
      },
      {
        value: 'Цена по возрастанию',
        code: '2'
      },
      {
        value: 'Новое',
        code: '3'
      }
    ]
  }

  const colorConfig = {
    title: 'Цвет',
    options: (props.options && props.options.colors) ? props.options.colors.map(color => translate.find(data => data.value === color)) : []
  }

  const sizeConfig = {
    title: 'Размер',
    options: (props.options && props.options.sizes) ? props.options.sizes.map(size => {
      return {
        value: size
      }
    }) : []
  }

  const priceConfig = {
    currency: 'руб.',
    min: (props.options && props.options.price) ? props.options.price.min : 0,
    max: (props.options && props.options.price) ? props.options.price.max : 1000000
  }

  const onSortChange = (val) => {
    setSP((state) => {
      return { ...state, sort: state.sort === val ? undefined : val }
    })
  }

  const arrayStateAction = (state, val, field) => {
    return { ...state, [field]: state[field] ? state[field].includes(val) ? state[field].filter(item => item !== val) : [ ...state[field], val ] : [ val ] }
  }

  const onColorChange = (val) => {
    setSP((state) => {
      return arrayStateAction(state, val, 'colors')
    })
  }

  const onSizeChange = (val) => {
    setSP((state) => {
      return arrayStateAction(state, val, 'sizes')
    })
  }

  const onPriceChange = (range) => {
    setSP(state => {
      return { ...state, price: range }
    })
  }

  return (
    <div className={style.search_parameters}>
      <Select
        title={sortConfig.title}
        options={sortConfig.options}
        onOption={onSortChange}
        activeValue={SP.sort}
      />
      <Price config={priceConfig} onPriceChangeComplete={onPriceChange} />
      <Select
        className={style.sp__size}
        title={sizeConfig.title}
        options={sizeConfig.options}
        onOption={onSizeChange}
        activeValue={SP.sizes || []}
        multiply
      />
      <Select
        title={colorConfig.title}
        options={colorConfig.options}
        onOption={onColorChange}
        activeValue={SP.colors || []}
        multiply
      />
    </div>
  )
}

SearchParameters.displayName = 'SearchParameters'

SearchParameters.propTypes = {
  onSearchChange: PropTypes.func
}

export default memo(SearchParameters, (prev, next) => {
  return (prev.onSearchChange.toString() === next.onSearchChange.toString()) && (JSON.stringify(prev.options) === JSON.stringify(next.options))
})
