import { memo } from 'react'
import style from './Price.module.scss'
import PropTypes from 'prop-types'
import 'react-input-range/lib/css/index.css'
import Range from '../../Range'

const Price = (props) => {
  const { config, onPriceChangeComplete, onPriceChange } = props

  const handleChange = (value) => {
    if (onPriceChange) {
      onPriceChange(value)
    }
  }

  const handleChangeComplete = (val) => {
    if (onPriceChangeComplete) {
      onPriceChangeComplete(val)
    }
  }

  const formatLabel = (val) => {
    return val + ' ' + config.currency
  }

  return (
    <div className={style.price_component}>
      <Range
        onChangeComplete={handleChangeComplete}
        onChange={handleChange}
        max={config.max}
        min={config.min}
        formatLabel={formatLabel}
      />

    </div>
  )
}

Price.propTypes = {
  config: PropTypes.shape({
    min: PropTypes.number,
    max: PropTypes.number,
    currency: PropTypes.string
  }),
  onPriceChangeComplete: PropTypes.func,
  onPriceChange: PropTypes.func
}

export default memo(Price)
