import { withTranslation } from '../../../i18n'
import { useState } from 'react'
import { useRouter } from 'next/router'
import ButtonPrimary from '../ButtonPrimary'
import Slider from '../Slider'
import style from './ProductContent.module.scss'
import classnames from '../../../utils/classnames'
import { Basket } from '../Icons'
import { useDispatch, useSelector } from 'react-redux'
import ButtonColor from '../ButtonColor'
import { APP_BASKETS_ADD_PRODUCT } from '../../../store/app/baskets/actionTypes'
import PopUp from '../PopUp'
import { toast } from 'react-toastify'
import Forms from '../../Header/Forms'


const ProductContent = (props) => {
  const [selectedOptions, setSelectedOptions] = useState({ color: '', size: '' })
  const [showPopUp, setShowPopUp] = useState('')
  const isAuth = useSelector(state => state.auth?.token)
  const baskets = useSelector(state => state.app.baskets)
  const { t } = props
  const router = useRouter()
  const dispatch = useDispatch()
  const product = useSelector(state => state.app.products.product[router.query.id] || {})

  const eachColor = (color, i) => (
    <ButtonColor
      className={classnames(selectedOptions.color === color ? style.colors__btn_active : null)}
      color={color}
      onClick={() => setSelectedOptions({ ...selectedOptions, color: color })}
      key={i}
    />
  )

  const eachSize = (size, i) => (
    <ButtonPrimary
      onClick={() => setSelectedOptions({ ...selectedOptions, size: size })}
      key={i}
      className={classnames(style.sizes__btn, selectedOptions.size === size ? style.sizes__btn_active : null)}
    >
      {size}
    </ButtonPrimary>
  )

  // const eachImage = (img, i) => (
  //   <div className={style.img} key={i}><img src={img}  alt={''}/></div>
  // )

  const getColors = () => product && product.colors && product.colors.map(eachColor)
  const getSizes = () => product && product.sizes && product.sizes.map(eachSize)
  // const getImages = () => product && product.images && product.images.map(eachImage)

  const putToBasket = (basketId) => {
    // console.log('SHOW', isAuth)
    if (isAuth) {
      setShowPopUp('BASKET')
      // console.log('Выбор корзины')
    } else {
      const currentBasketId = basketId ? basketId : baskets.find(b => b.main === true).id
      dispatch({ type: APP_BASKETS_ADD_PRODUCT, product: product, basketId: currentBasketId, options: selectedOptions })
      setShowPopUp('')
      // console.log('В главную корзину')
    }
  }

  const putToBasketLogin = (basketId) => {
    dispatch({ type: APP_BASKETS_ADD_PRODUCT, product: product, basketId: basketId, options: selectedOptions })
    setShowPopUp('')
  }

  const isDisabled = () => {
    const isSelectOptions = () => {
      if (product.category !== 'accessories') {
        return !(selectedOptions.color && selectedOptions.size)
      } else {
        return !selectedOptions.color
      }
    }
    if (isAuth) {
      return isSelectOptions()
    } else {
      return !!baskets.find(b => b.main).products?.find(prod => prod.id === product.id) || isSelectOptions()
    }
  }

  const getTextAddToBasket = () => {
    if (isAuth) {
      return 'Добавить в корзину'
    } else if (baskets.find(b => b.main).products?.find(prod => prod.id === product.id)) {
      return 'Добавлено'
    }
  }

  const quickBuy = (success) => {
    console.log('AFTER', isAuth)
    if (isAuth || success === true) {
      toast.success('Товар успешно куплен')
    } else {
      setShowPopUp('POP_UP_REGISTER')
    }
  }

  const unit = '₽'
  console.log(isAuth)

  return (
    <div className={style.product_content}>
      {product ? (
        <>
          <div className={classnames(style.product_content__pc_slider, style.pc_slider)}>
            <div className={''}>
              <Slider dots={true} arrows={false} lazyLoad={true} >
                {/*{getImages()}*/}
                <div className={style.img}><img src={product.image} alt={''}/></div>
              </Slider>
            </div>
          </div>
          <div className={classnames(style.product_content__pc_full_info, style.pc_full_info)}>
            <div className={style.title}>{product.name}</div>
            <div className={style.price_box}>
              <div className={style.price}>{product.price + ' ' + unit}</div>
              {product.priceOld ? <div className={style.old_price}>{product.priceOld + ' ' + unit}</div> : null}
            </div>
            {product.description && (
              <div>
                <div className={style.subtitle}>{t('description')}</div>
                <div className={style.label}>{product.description}</div>
              </div>)
            }
            <div>
              <div className={style.subtitle}>{t('colors')}</div>
              <div className={style.colors}>{getColors()}</div>
            </div>
            <div>
              <div className={style.subtitle}>{t('sizes')}</div>
              {product.category !== 'accessories' && <div className={style.sizes}>{getSizes()}</div>}
            </div>
            <div className={style.buttons_box}>
              <ButtonPrimary className={style.btn_buy} onClick={quickBuy} disabled={isDisabled()}>
                {/*<Basket />*/}
                {/*<span>{getTextAddToBasket()}</span>*/}
                <span>Быстрая покупка</span>
              </ButtonPrimary>
              <ButtonPrimary className={style.btn_basket} onClick={putToBasket} disabled={isDisabled()}>
                <Basket />
              </ButtonPrimary>
            </div>
          </div>
        </>
      ) : (
        <div>Такого продукта нет</div>
      )}
      <Forms
        changePopUp={() => setShowPopUp('')}
        showPopUp={showPopUp === 'POP_UP_REGISTER' ? 'POP_UP_REGISTER' : ''}
        afterRegister={quickBuy}
        quickBuy
      />
      <PopUp show={showPopUp === 'BASKET'} onClose={() => setShowPopUp('')}>
        <div className={style.pop_up__container}>
          {/*<ButtonPrimary onClick={() => putToBasketLogin('main')} className={style.basket}>{'Моя корзина'}</ButtonPrimary>*/}
          {baskets ? baskets.map(basket => (
            <ButtonPrimary key={basket.id} onClick={() => putToBasketLogin(basket.id)} className={style.basket}>{basket.name}</ButtonPrimary>
          )) : null}
        </div>

      </PopUp>
    </div>
  )
}

ProductContent.defaultProps = {
  i18nNamespaces: ['productContent']
}

const Page = withTranslation('productContent')(ProductContent)

export default Page
