import React, { useState, memo, useEffect } from 'react'
import mockupStock from '../../../public/mockups/mockupStock.png'
import PropTypes from 'prop-types'
import style from './productCard.module.scss'
import classnames from '../../../utils/classnames'
import { useRouter } from 'next/router'

const ProductCard = memo(({ product }) => {
  const [loading, setLoading] = useState(true)
  const [src, setSrc] = useState('')
  const router = useRouter()
  useEffect(() => {
    setSrc(product.image || mockupStock)
  }, [product])

  const goToProduct = (id) => router.push('/catalog/' + product.category + '/' + id)

  const unit = '₽'

  return (
    <div className={classnames(style.product_card)} onClick={() => goToProduct(product.id)}>
      <div className={classnames(style.pc__loader, loading ? style.pc__loader_active : '')} />
      <div className={classnames(style.pc_deck, loading ? style.pc_deck__loading : '')}>
        <div className={style.pc__image}>
          <img onLoad={() => setLoading(false)} src={src} decoding={'async'} alt={''}/>
        </div>
        <div className={style.pc__name}>
          {product.name}
        </div>
        <div className={style.pc__product_price}>
          {product.priceOld ? (
            <>
              <div>{product.price + ' ' + unit}</div>
              <div className={style.product_price__old}>{product.priceOld + ' ' + unit}</div>
            </>
          ) : (
            <div>{product.price + ' ' + unit}</div>
          )}
        </div>
      </div>



    </div>
  )
})

ProductCard.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.string,
    image: PropTypes.string,
    category: PropTypes.string,
    priceOld: PropTypes.number,
    price: PropTypes.number,
    name: PropTypes.string
  }).isRequired
}

ProductCard.displayName = 'ProductCard'

export default ProductCard
