import { useMemo } from 'react'
import PropTypes from 'prop-types'
import style from './GridWrapper.module.scss'
import classnames from '../../../utils/classnames'

const GridWrapper = ({ children, className, columns = 10 }) => {
  const width = useMemo(() => {
    return 100 / 12 * columns
  }, [columns])

  return (
    <div className={classnames(style.grid, className)}>
      <div
        className={classnames(style.grid__container)}
        // style={{ width: `${width}%` }}
      >
        {children}
      </div>
    </div>
  )
}

GridWrapper.propTypes = {
  columns: PropTypes.number,
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.func]).isRequired
}

export default GridWrapper
