import { memo, useState, useMemo } from 'react'
import PropTypes from 'prop-types'
import style from './Select.module.scss'
import ButtonPrimary from '../ButtonPrimary'
import classnames from '../../../utils/classnames'
import getRandomId from '../../../utils/getRandomId'

const Select = (props) => {
  const [showList, setShowList] = useState(false)
  const isAnySelected = useMemo(() => {
    if (props.multiply) {
      return !!props.options.find((option) =>  props.activeValue.includes(option.value))
    } else {
      return !!props.options.find((option) => option.value === props.activeValue)
    }

  }, [props.activeValue, props.multiply, props.options])

  const id = useMemo(() => {
    return {
      box: getRandomId(),
      btn: getRandomId()
    }
  }, [])

  const showOpacityOption = (id) => {
    let block = document.querySelector(id)
    if (block) {
      block.style.opacity = 1
    }
  }

  const handleOptionClick = (value) => {
    if (props.onOption) {
      props.onOption(value)
    }
  }

  const isActive = (value) => {
    if (props.multiply) {
      return props.activeValue.includes(value)
    } else {
      return props.activeValue === value
    }
  }

  const eachOption = (option, i) => {
    setTimeout(() => showOpacityOption(`#${style.select_box__option + '-' + i}`), i * 100)
    return (
      <div
        key={i}
        className={classnames(style.select_box__option, isActive(option.value) && style.select_box__option_active)}
        id={style.select_box__option + '-' + i}
        onClick={() => handleOptionClick(option.value)}
      >
        {(option.content && option.content()) || (option.text || option.value)}
      </div>
    )
  }

  const showBox = () => {
    let flag
    if (showList) {
      flag = false
    } else {
      flag = true
      const focus = () => {
        let box = document.querySelector(`#${id.box}`)
        if (box) {
          box.focus()
        }
      }
      setTimeout(focus, 0)
    }
    setShowList(flag)
  }

  const hideBox = (e) => {
    if ((e.relatedTarget && e.relatedTarget.id) !== id.btn) {
      setShowList(false)
    }
  }

  const showChildren = () => props.options && props.options.map(eachOption)

  return (
    <div className={classnames(style.select_component, props.className)}>
      <ButtonPrimary className={classnames(style.select_btn, isAnySelected && style.select_btn__active)} onClick={showBox} id={id.btn}>
        {props.title || 'Выбрать'}
      </ButtonPrimary>
      {showList && (
        <div className={classnames(style.select_box)} id={id.box} tabIndex={0} onBlur={hideBox}>
          {showChildren()}
        </div>)
      }
    </div>
  )
}

Select.propTypes = {
  onOption: PropTypes.func.isRequired,
  title: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.shape({
    content: PropTypes.function,
    value: PropTypes.any
  })).isRequired,
  className: PropTypes.string,
  multiply: PropTypes.bool,
  activeValue: PropTypes.oneOfType([PropTypes.any, PropTypes.array])
}

export default memo(Select)
