import PropTypes from 'prop-types'
import classnames from '../../../utils/classnames'
import style from './Input.module.scss'

const Input = ({ classNames = {}, icon, label, message, ...rest }) => {
  return (
    <div className={classnames(style.input_container, classNames.box)}>
      {label && <div className={style.input_container__label}>{label}</div>}
      <div className={style.input_box}>
        <input className={classnames(style.input, classNames.input)} { ...rest } />
        {icon && <div className={style.input_box__icon}>{icon}</div>}
      </div>
      <div className={style.input_container__sublabel}>{message}</div>
    </div>
  )
}

Input.propTypes = {
  icon: PropTypes.node,
  label: PropTypes.string,
  message: PropTypes.string,
  classNames: PropTypes.shape({
    box: PropTypes.string,
    input: PropTypes.string
  })
}

export default Input
