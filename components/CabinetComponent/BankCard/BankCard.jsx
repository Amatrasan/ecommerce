import { memo } from 'react'
import style from './BankCard.module.scss'
import PropTypes from 'prop-types'
import { Chip, Delete } from '../../UI/Icons'
import classnames from '../../../utils/classnames'
import ButtonPrimary from '../../UI/ButtonPrimary'

const BankCard = ({ card = {}, onDelete }) => {
  return (
    <div className={style.bank_card}>
      <ButtonPrimary className={style.bc__btn} onClick={() => onDelete(card)}><Delete/></ButtonPrimary>
      <div className={style.bc__background} />
      <div className={style.bc__payment_system}>VISA</div>
      <div className={classnames(style.bc__numbers, style.bc__numbers_box)}>
        {card.number}
      </div>
      <div className={classnames(style.bc__numbers, style.bc__date_box)}>{card.date}</div>
      <div className={style.bc__chip}><Chip /></div>
    </div>
  )
}

BankCard.propTypes = {
  card: PropTypes.shape({
    number: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    date: PropTypes.string
  }).isRequired,
  onDelete: PropTypes.func.isRequired
}

export default memo(BankCard)
