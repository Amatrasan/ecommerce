import { useState, useEffect } from 'react'
import style from './Cabinet.module.scss'
import ButtonPrimary from '../UI/ButtonPrimary'
import classnames from '../../utils/classnames'
import Common from './Common'
import Basket from './Basket'
import { useRouter } from 'next/router'

const Cabinet = () => {
  const [currentTab, setCurrentTab] = useState('COMMON')
  const router = useRouter()

  useEffect(() => {
    if (router.asPath.match('basket')) {
      setCurrentTab('BASKET')
    }
  },[router])

  const getBtnStyle = (tab) => classnames(style.btn, currentTab === tab ? style.btn_active : '')

  return (
    <div className={style.cabinet}>
      <div className={classnames(style.cabinet__c_nav_list, style.c_nav_list)}>
        <ButtonPrimary className={getBtnStyle('COMMON')} onClick={() => setCurrentTab('COMMON')}>
          Общая информация
        </ButtonPrimary>
        <ButtonPrimary className={getBtnStyle('BASKET')} onClick={() => setCurrentTab('BASKET')}>
          Корзина
        </ButtonPrimary>
        {/*<ButtonPrimary className={getBtnStyle('HISTORY')} onClick={() => setCurrentTab('HISTORY')}>*/}
        {/*  История покупок*/}
        {/*</ButtonPrimary>*/}
        {/*<ButtonPrimary className={getBtnStyle('FAVOURITES')} onClick={() => setCurrentTab('FAVOURITES')}>*/}
        {/*  Избранное*/}
        {/*</ButtonPrimary>*/}
      </div>
      <div className={style.cabinet__tab_container}>
        {currentTab === 'COMMON' && <Common />}
        {currentTab === 'BASKET' && <Basket />}
      </div>
    </div>
  )
}

export default Cabinet
