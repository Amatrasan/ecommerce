import { useState } from 'react'
import style from './Basket.module.scss'
import ButtonPrimary from '../../UI/ButtonPrimary'
import { Human, Delete } from '../../UI/Icons'
import { useRouter } from 'next/router'
import PopUp from '../../UI/PopUp'
import { useDispatch, useSelector } from 'react-redux'
import Input from '../../UI/Input'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import {
  APP_BASKET_ADD_HUMAN_IN_BASKET, APP_BASKET_BUY, APP_BASKET_DELETE_HUMAN_IN_BASKET,
  APP_BASKET_DELETE_PRODUCT_IN_BASKET,
  APP_BASKETS_CREATE_BASKET,
  APP_BASKETS_DELETE_BASKET,
  APP_BASKETS_GET_BASKET_PRODUCTS
} from '../../../store/app/baskets/actionTypes'
import ButtonColor from '../../UI/ButtonColor'

const popUpTypes = {
  POP_UP_DELETE_PRODUCT: 'POP_UP_DELETE_PRODUCT',
  POP_UP_DELETE_BASKET: 'POP_UP_DELETE_BASKET',
  POP_UP_LEFT_BASKET: 'POP_UP_LEFT_BASKET',
  POP_UP_SHOW_HUMAN: 'POP_UP_SHOW_HUMAN',
  POP_UP_CREATE_BASKET: 'POP_UP_CREATE_BASKET'
}

const Basket = () => {
  const router = useRouter()
  const dispatch = useDispatch()
  const [state, setState] = useState({ currentBasket: {}, currentProduct: {} })
  const [popUp, setPopUp] = useState('')
  const baskets = useSelector(state => state.app.baskets)

  const formCreateBasket = useFormik({
    initialValues: {
      name: '',
    },
    validationSchema: Yup.object({
      name: Yup.string().required('Введите название корзины'),
    }),
    onSubmit: (values, formik) => {
      dispatch({ type: APP_BASKETS_CREATE_BASKET, name: values.name })
      closePopUp()
      formik.resetForm()
    }
  })

  const getBasket = (basketId) => {
    dispatch({ type: APP_BASKETS_GET_BASKET_PRODUCTS, basketId })
  }

  const deleteProductInBasket = () => {
    dispatch({ type: APP_BASKET_DELETE_PRODUCT_IN_BASKET, basketId: state.currentBasket.id, productId: state.currentProduct.id })
    closePopUp()
  }

  const deleteBasket = () => {
    dispatch({ type: APP_BASKETS_DELETE_BASKET, basketId: state.currentBasket.id })
    setState({ ...state, currentBasket: {} })
    closePopUp()
  }

  const eachBasket = (basket, i) => (
    <div key={i} className={style.btn_box} onClick={() => setState({ ...state, currentBasket: basket })}>
      <ButtonPrimary className={style.btn} onClick={() => getBasket(basket.id)}>
        {basket.name}
      </ButtonPrimary>
      {(basket.main !== true) && (
        <div className={style.btn_box__tooltip}>
          <ButtonPrimary className={style.btn_more} onClick={() => setPopUp(popUpTypes.POP_UP_SHOW_HUMAN)}><Human/></ButtonPrimary>
          {basket.owner && <ButtonPrimary className={style.btn_more} onClick={() => setPopUp(popUpTypes.POP_UP_DELETE_BASKET)}><Delete/></ButtonPrimary>}
        </div>)
      }
    </div>
  )

  const showBaskets = () => baskets && baskets.map(eachBasket)

  const eachProduct = (product, i) => (
    <div className={style.product} key={i} onClick={() => setState({ ...state, currentProduct: product })}>
      <div className={style.product__img_container}>
        <img src={product.image} alt={product.name} />
      </div>
      <div className={style.product__p_information}>
        {(product.customer || state.currentBasket.owner) && <ButtonPrimary className={style.p_information__delete_btn} onClick={() => setPopUp(popUpTypes.POP_UP_DELETE_PRODUCT)}><Delete /></ButtonPrimary>}
        <div className={style.p_information__name} onClick={() => goToProduct('shoes',product.id)}>{product.name}</div>
        <div className={style.p_information__footer}>
          <div className={style.p_information__label}>Размер: <b className={style.p_information__name}>{product.size}</b></div>
          <div className={style.p_information__label}>Цвет: <ButtonColor color={product.color} /></div>
          <div className={style.p_information__label}>Цена: <b className={style.p_information__name}>{product.price + ' ' + '₽'}</b></div>
        </div>
      </div>
    </div>
  )

  const goToProduct = (category, id) => router.push(`/catalog/${category}/${id}`)


  const closePopUp = () => setPopUp('')

  const showProductsInBasket = () => {
    if (state.currentBasket) {
      const getBasket = (baskets) => {
        return baskets && baskets.find(basket => basket.id === state.currentBasket.id)
      }
      const currentBasket = getBasket(baskets)
      return currentBasket && currentBasket.products && currentBasket.products.map(eachProduct)
    } else {
      return null
    }
  }

  const deleteHumanInBasket = (userId, basketId) => {
    dispatch({ type: APP_BASKET_DELETE_HUMAN_IN_BASKET, basketId: basketId, userId: userId })
  }

  const getHumansInBasket = () => {
    if (state.currentBasket && state.currentBasket.users) {
      return baskets.find(b => b.id === state.currentBasket.id)?.users.map(user => (
        <div key={user.id} className={style.human}>
          <div className={style.name}>{user.firstName}</div>
          <div className={style.name}>{user.lastName}</div>
          <div className={style.patronymic}>{user.patronymic ? user.patronymic[0] : ''}</div>
          {state.currentBasket.owner && <ButtonPrimary className={style.delete} onClick={() => deleteHumanInBasket(user.id, state.currentBasket.id)}><Delete /></ButtonPrimary>}
        </div>
      ))
    }
  }

  const [popUpAddHuman, setPopUpAddHuman] = useState(false)
  const [humanId, setHumanId] = useState('')

  const addHumanInBasket = () => {
    dispatch({ type: APP_BASKET_ADD_HUMAN_IN_BASKET, basketId: state.currentBasket.id, userId: humanId })
    setHumanId('')
    setPopUpAddHuman(false)
  }

  const buyBasket = () => {
    dispatch({ type: APP_BASKET_BUY, basketId: state.currentBasket.id })
  }

  return (
    <div className={style.basket}>
      <div className={style.basket__b_left_corner}>
        <div className={style.basket_list}>
          {showBaskets()}
        </div>
        <div className={style.line} />
        <ButtonPrimary className={style.btn} onClick={() => setPopUp(popUpTypes.POP_UP_CREATE_BASKET)}>
          +
        </ButtonPrimary>
      </div>
      <div className={style.basket__b_right_corner}>
        {showProductsInBasket()}
        {(state.currentBasket.products && baskets.find(b => b.id === state.currentBasket.id).products.length && (baskets.find(b => b.id === state.currentBasket.id).owner || baskets.find(b => b.id === state.currentBasket.id).main)) ? <ButtonPrimary className={style.btn_medium} onClick={buyBasket}>Купить</ButtonPrimary> : null}

      </div>
      <PopUp show={popUp === popUpTypes.POP_UP_DELETE_PRODUCT} onClose={closePopUp} title={'Удалить товар из корзины'}>
        <div className={style.subtitle}>
          {'Вы точно хотите удалить товар '}
          <b>{state.currentProduct.name}</b>
          {' ?'}
        </div>
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <ButtonPrimary onClick={deleteProductInBasket} className={style.btn_medium}>Да</ButtonPrimary>
          <ButtonPrimary onClick={closePopUp} className={style.btn_medium}>Нет</ButtonPrimary>
        </div>
      </PopUp>
      <PopUp show={popUp === popUpTypes.POP_UP_DELETE_BASKET} title={'Удалить корзину'} onClose={closePopUp}>
        <div className={style.subtitle}>
          {'Вы точно хотите удалить корзину '}
          <b>{state.currentBasket.name}</b>
          {' ?'}
        </div>
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <ButtonPrimary onClick={deleteBasket} className={style.btn_medium}>Да</ButtonPrimary>
          <ButtonPrimary onClick={closePopUp} className={style.btn_medium}>Нет</ButtonPrimary>
        </div>
      </PopUp>
      <PopUp show={popUp === popUpTypes.POP_UP_CREATE_BASKET} onClose={closePopUp} title={'Создать корзину'}>
        <div className={style.subtitle}>Введите название корзины</div>
        <form onSubmit={formCreateBasket.handleSubmit}>
          <Input
            name={'name'}
            value={formCreateBasket.values.name}
            onChange={formCreateBasket.handleChange}
            message={formCreateBasket.errors.name}
          />
          <ButtonPrimary type={'submit'} className={style.btn_medium}>Создать</ButtonPrimary>
        </form>
      </PopUp>
      <PopUp show={popUp === popUpTypes.POP_UP_SHOW_HUMAN} onClose={closePopUp} title={'Участники корзины'}>
        <div className={style.pop_up_humans}>
          {getHumansInBasket()}
        </div>
        <ButtonPrimary className={style.btn_medium} onClick={() => setPopUpAddHuman(true)}>Добавить человека</ButtonPrimary>
      </PopUp>
      <PopUp show={popUpAddHuman} onClose={() => setPopUpAddHuman(false)} title={'Добавить участника в корзину'}>
        <Input vale={humanId} onChange={e => setHumanId(e.target.value)} />
        <ButtonPrimary className={style.btn_medium} disabled={!humanId} onClick={addHumanInBasket}>Добавить</ButtonPrimary>
      </PopUp>
    </div>
  )
}

export default Basket
