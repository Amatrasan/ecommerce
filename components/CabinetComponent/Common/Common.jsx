import { useEffect, useState } from 'react'
import Input from '../../UI/Input'
import style from './Common.module.scss'
import { Copy } from '../../UI/Icons'
import BankCard from '../BankCard/BankCard'
import ButtonPrimary from '../../UI/ButtonPrimary'
import Slider from '../../UI/Slider'
import { useDispatch, useSelector } from 'react-redux'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import { APP_USER_CARD_CREATE, APP_USER_CARD_DELETE, APP_USER_UPDATE } from '../../../store/user/actionTypes'
import PopUp from '../../UI/PopUp'

const Cabinet = {
  Common: {
    privateInfo: 'Личные данные'
  }
}

const Common = () => {
  const user = useSelector(state => state.user)
  const dispatch = useDispatch()
  const [state, setState] = useState({ currentCard: {}, popUpType: '' })

  const sliderConfig = {
    slidesToShow: user && user.cards && user.cards.length > 1 ? 2 : 1,
    slidesToScroll: 1,
    infinite: true,
    initialSlide: 1,
    swipeToSlide: true,
    dots: false,
    arrows: false,
  }

  const formUser = useFormik({
    initialValues: {
      id: '',
      email: '',
      phone: '',
      lastName: '',
      firstName: '',
      patronymic: '',
      country: '',
      city: '',
      address: ''
    },
    validationSchema: Yup.object({
      email: Yup.string()
        .email('Email введён неверно')
        .trim()
        .required('Введите Email'),
      phone: Yup.string()
        .required('Введите номер телефона')
        .test('min', 'Не менее 11 символов', val => val && val.toString().length >= 11),
      id: Yup.string().required(),
      lastName: Yup.string().required('Введите фамилию'),
      firstName: Yup.string().required('Введите имя'),
      patronymic: Yup.string(),
      country: Yup.string().required('Введите страну'),
      city: Yup.string().required('Введите город'),
      address: Yup.string().required('Введите адрес')
    }),
    onSubmit: values => dispatch({ type: APP_USER_UPDATE, user: values })
  })

  const formCard = useFormik({
    initialValues: {
      number: '',
      date: ''
    },
    validationSchema: Yup.object({
      date: Yup.string().required('Введите дату в формате (01/20)'),
      number: Yup.number()
        .required('Введите номер карты')
        .test('min', 'Введите 16 символов', val => val && val.toString().length === 16)
    }),
    onSubmit: (values, formik) => {
      dispatch({ type: APP_USER_CARD_CREATE, card: values })
      closePopUp()
      formik.resetForm()
    }
  })

  useEffect(() => {
    const userNotRole = { ...user }
    delete userNotRole.role
    delete userNotRole.cards
    if (user && (user.id !== formUser.values.id)) {
      formUser.setValues(userNotRole)
    }
  }, [formUser, user])

  const copyToClipboard = (text) => navigator.clipboard.writeText(text)

  const showDialogCardDelete = (card) => {
    setState({ currentCard: card, popUpType: 'DELETE_CARD' })
  }

  const deleteCard = () => {
    dispatch({ type: APP_USER_CARD_DELETE, cardId: state.currentCard.id })
    setState({ currentCard: {}, popUpType: '' })
  }

  const showCards = () => user && user.cards && user.cards.map(card => <BankCard card={card} key={card.id} onDelete={showDialogCardDelete} />)

  const closePopUp = () => setState({ ...state, popUpType: '' })

  return (
    <div className={style.common}>
      <form onSubmit={formUser.handleSubmit}>
        <div className={style.subtitle}>{Cabinet.Common.privateInfo}</div>
        <div className={style.flex}>
          <Input
            name={'id'}
            icon={<div style={{ cursor: 'pointer' }} onClick={() => copyToClipboard(formUser.values.id)}><Copy/></div>}
            label={'ID'}
            classNames={{ box: style.input__id }}
            readOnly={true}
            value={formUser.values.id}
            message={formUser.errors.id}
          />
          <Input
            name={'email'}
            icon={null}
            label={'Email'}
            classNames={{ box: style.input__email }}
            value={formUser.values.email}
            onChange={formUser.handleChange}
            message={formUser.errors.email}
          />
          <Input
            name={'phone'}
            icon={null}
            label={'Номер телефона'}
            value={formUser.values.phone}
            onChange={formUser.handleChange}
            message={formUser.errors.phone}
            classNames={{ box: style.input__phone }}
          />
        </div>
        <div className={style.flex}>
          <Input
            name={'lastName'}
            icon={null}
            label={'Фамилия'}
            classNames={{ box: style.input__third }}
            value={formUser.values.lastName}
            onChange={formUser.handleChange}
            message={formUser.errors.lastName}
          />
          <Input
            name={'firstName'}
            icon={null}
            label={'Имя'}
            classNames={{ box: style.input__third }}
            value={formUser.values.firstName}
            onChange={formUser.handleChange}
            message={formUser.errors.firstName}
          />
          <Input
            name={'patronymic'}
            icon={null}
            label={'Отчество'}
            classNames={{ box: style.input__third }}
            value={formUser.values.patronymic}
            onChange={formUser.handleChange}
            message={formUser.errors.patronymic}
          />
        </div>
        <div className={style.flex}>
          <Input
            name={'country'}
            icon={null}
            label={'Страна'}
            classNames={{ box: style.input__fifth }}
            value={formUser.values.country}
            onChange={formUser.handleChange}
            message={formUser.errors.country}
          />
          <Input
            name={'city'}
            icon={null}
            label={'Город'}
            classNames={{ box: style.input__fifth }}
            value={formUser.values.city}
            onChange={formUser.handleChange}
            message={formUser.errors.city}
          />
        </div>
        <Input
          name={'address'}
          icon={null}
          label={'Адрес'}
          value={formUser.values.address}
          onChange={formUser.handleChange}
          message={formUser.errors.address}
        />
        <ButtonPrimary className={style.btn_save} type={'submit'}>Сохранить</ButtonPrimary>
      </form>
      <div className={style.subtitle}>{'Привязанные карты'}</div>
      <div className={style.slider_box}>
        <div className={style.slider_box__sb_container}>
          <Slider { ...sliderConfig } className={style.slider} >
            {showCards()}
          </Slider>
        </div>

        <ButtonPrimary
          type={'button'}
          className={style.btn}
          onClick={() => setState({ ...state, popUpType: 'CREATE_CARD' })}
        >
          +
        </ButtonPrimary>
      </div>
      <PopUp show={state.popUpType === 'DELETE_CARD'} title={'Удалить банковскую карту'} onClose={closePopUp}>
        <div>Вы действительно хотите удалить карту?</div>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <ButtonPrimary onClick={deleteCard} className={style.btn_medium}>Да</ButtonPrimary>
          <ButtonPrimary onClick={closePopUp} className={style.btn_medium}>Нет</ButtonPrimary>
        </div>
      </PopUp>
      <PopUp show={state.popUpType === 'CREATE_CARD'} title={'Добавить карту'} onClose={closePopUp}>
        <form onSubmit={formCard.handleSubmit}>
          <Input name={'number'} label={'Номер карты'} value={formCard.values.number} onChange={formCard.handleChange} message={formCard.errors.number}/>
          <Input name={'date'} label={'Срок действия (ММ/ДД)'} maxLength={5} value={formCard.values.date} onChange={formCard.handleChange} message={formCard.errors.date} />
          <ButtonPrimary type={'submit'} className={style.btn_save}>Создать</ButtonPrimary>
        </form>
      </PopUp>

    </div>
  )
}

export default Common
