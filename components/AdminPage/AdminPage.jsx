import { useState } from 'react'
import style from './AdminPage.module.scss'
import ButtonPrimary from '../UI/ButtonPrimary'
import classnames from '../../utils/classnames'
import Products from './Products'
import Stocks from './Stocks'

const AdminPage = () => {
  const [currentTab, setCurrentTab] = useState('PRODUCTS')

  const getBtnStyle = (tab) => classnames(style.btn, currentTab === tab ? style.btn_active : '')

  return (
    <div className={style.admin_page}>
      <div className={classnames(style.ap__nav_header, style.nav_header)}>
        <ButtonPrimary className={getBtnStyle('PRODUCTS')} onClick={() => setCurrentTab('PRODUCTS')}>Товары</ButtonPrimary>
        <ButtonPrimary className={getBtnStyle('STOCKS')} onClick={() => setCurrentTab('STOCKS')}>Акции</ButtonPrimary>
      </div>
      <div>
        {currentTab === 'PRODUCTS' && <Products />}
        {currentTab === 'STOCKS' && <Stocks />}
      </div>
    </div>
  )
}

export default AdminPage
