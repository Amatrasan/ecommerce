import { useEffect, useState } from 'react'
import style from './ProductForm.module.scss'
import PropTypes from 'prop-types'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import ButtonPrimary from '../../../UI/ButtonPrimary'
import Input from '../../../UI/Input'
import classnames from '../../../../utils/classnames'
import ButtonColor from '../../../UI/ButtonColor'

const text = {
  btnCreate: 'Создать',
  btnUpdate: 'Обновить'
}


const ProductForm = ({ product, onCreate, onUpdate }) => {
  const [image, setImage] = useState({})

  const form = useFormik({
    initialValues: {
      image: '',
      name: '',
      price: '',
      priceOld: '',
      colors: [],
      size: '',
      count: '',
      category: ''
    },
    validationSchema: Yup.object({
      image: Yup.string().required('Загрузите изображение'),
      name: Yup.string().required('Введите название продукта'),
      price: Yup.number().typeError('Должно быть число').positive('Положительное число').required('Введите цену продукта'),
      priceOld: Yup.number().typeError('Должно быть число').positive('Положительное число'),
      colors: Yup.array().min(1, 'Необходимо добавить цвет'),
      category: Yup.string().required('Необходимо выбрать категорию'),
      size: Yup.string().required('Необходимо добавить размер').when(['category'], (category) => {
        if (category === 'accessories') {
          return Yup.string().nullable()
        }
      }),
      count: Yup.number().test('pos', 'Не отрицательное число',(val) => val >= 0).required('Необходимо заполнить колличество'),

    }),
    onSubmit: (values) => {
      if (product && product.id) {
        onUpdate({ id: product.id, ...values, imgFile: image.file })
      } else {
        onCreate({ ...values, imgFile: image.file })
      }

    }
  })

  useEffect(() => {
    if (product && form.values.image === '') {
      form.setValues({
        name: product.name,
        price: product.price,
        category: product.category,
        size: product.size,
        priceOld: product.priceOld,
        image: product.image,
        colors: product.colors,
        count: product.count
      })
      setImage({ ...image, result: product.image })
    }
  }, [form, product])

  const getSizes = () => {
    const sizes = []
    if (form.values.category === 'shoes') {
      for (let i = 36; i < 46; i++) {
        sizes.push(<ButtonPrimary
          type={'button'}
          key={i}
          onClick={() => form.setFieldValue('size', i)}
          className={classnames(form.values.size === i && style.btn_active)}
        >
          {i}
        </ButtonPrimary>)
      }

    } else if (form.values.category === 'shirts') {
      const clothesSizes = ['XS', 'S', 'M', 'L', 'XL', 'XXL']
      clothesSizes.forEach(size => {
        sizes.push(<ButtonPrimary
          key={size}
          type={'button'}
          className={classnames(form.values.size === size && style.btn_active)}
          onClick={() => form.setFieldValue('size', size)}
        >
          {size}
        </ButtonPrimary>)
      })
    }
    return sizes
  }

  const getColorButtons = () => {
    const colors = require('./../../../../dictionary/colors').default

    const array = []
    colors.forEach(color => {
      array.push(
        <ButtonColor
          key={color}
          color={color}
          type={'button'}
          className={form.values.colors.includes(color)? style.btn_active : null}
          onClick={() => form.setFieldValue('colors', [ color, ...form.values.colors])}
        />
      )
    })
    return array
  }

  const handleFileChange = (e) => {
    const file = e.target.files ? e.target.files[0] : e.dataTransfer.files[0]
    const fr = new FileReader()
    fr.onload = function () {
      setImage({ result: fr.result, file: file })
      form.setFieldValue('image', fr.result)
    }
    fr.readAsDataURL(file)
  }

  const getBtnTypeStyle = (tab) => classnames(style.btn_medium, form.values.category === tab ? style.btn_active : '')

  const changeCategory = (cat) => {
    form.setFieldValue('category', cat)
    form.setFieldValue('size', '')
  }

  return (
    <form onSubmit={form.handleSubmit} className={style.form}>
      <div className={style.flex}>
        <input type={'file'} className={style.input_file} onChange={handleFileChange}/>
        <div className={style.image_container}>
          {image.result ? <img src={image.result} alt={''} /> : null}
        </div>
        <ButtonPrimary className={style.btn_file} onClick={() => document.querySelector(`.${style.input_file}`).click()}>{'Загрузить картинку'}</ButtonPrimary>
      </div>

      <Input
        name={'name'}
        type={'text'}
        value={form.values.name}
        onChange={form.handleChange}
        message={form.errors.name}
        label={'Название продукта'}
      />
      <Input
        name={'price'}
        type={'number'}
        value={form.values.price}
        onChange={form.handleChange}
        message={form.errors.price}
        label={'Акутальная цена'}
      />
      <Input
        name={'priceOld'}
        type={'number'}
        value={form.values.priceOld}
        onChange={form.handleChange}
        message={form.errors.priceOld}
        label={'Старая цена'}
      />
      <Input
        name={'count'}
        type={'number'}
        value={form.values.count}
        onChange={form.handleChange}
        message={form.errors.count}
        label={'Колличество'}
      />
      <div className={style.flex}>
        <ButtonPrimary type={'button'} className={getBtnTypeStyle('shirts')} onClick={() => changeCategory('shirts')}>{'Рубашка'}</ButtonPrimary>
        <ButtonPrimary type={'button'} className={getBtnTypeStyle('shoes')} onClick={() => changeCategory('shoes')}>{'Обувь'}</ButtonPrimary>
        <ButtonPrimary type={'button'} className={getBtnTypeStyle('accessories')} onClick={() => changeCategory('accessories')}>{'Аксессуар'}</ButtonPrimary>
      </div>
      <div className={style.error}>{form.errors.category}</div>
      {form.values.category && (
        <>
          <div className={style.subtitle}>Размер</div>
          <div className={style.flex}>
            {getSizes()}
          </div>
          <div className={style.error}>{form.errors.size}</div>
        </>
      )}
      <div className={style.subtitle}>Цвет</div>
      <div className={style.flex}>
        {getColorButtons()}
      </div>
      <div className={style.error}>{form.errors.colors || form.errors.image}</div>
      <ButtonPrimary type={'submit'} className={style.btn_create}>{(product && product.id) ? text.btnUpdate : text.btnCreate}</ButtonPrimary>
    </form>
  )
}

ProductForm.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string,
    price: PropTypes.number,
    category: PropTypes.string,
    size: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    priceOld: PropTypes.number,
    image: PropTypes.string,
    colors: PropTypes.array,
    count: PropTypes.number
  }),
  onUpdate: PropTypes.func.isRequired,
  onCreate: PropTypes.func.isRequired
}

export default ProductForm
