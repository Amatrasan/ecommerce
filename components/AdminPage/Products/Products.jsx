import { useState, useEffect, useRef } from 'react'
import style from './Products.module.scss'
import ButtonPrimary from '../../UI/ButtonPrimary'
import classnames from '../../../utils/classnames'
import { useDispatch, useSelector } from 'react-redux'
import {
  ADMIN_PRODUCTS_GET,
  ADMIN_PRODUCTS_PRODUCT_CREATE,
  ADMIN_PRODUCTS_PRODUCT_UPDATE
} from '../../../store/admin/products/actionTypes'
import NeoWrapper from '../../UI/NeoWrapper'
import PopUp from '../../UI/PopUp'
import ProductForm from './ProductForm/ProductForm'
import ButtonColor from '../../UI/ButtonColor'
import api from '../../../services/ApiService'
import Input from '../../UI/Input'
import getRandomId from '../../../utils/getRandomId'
import { Delete } from '../../UI/Icons'

const Products = () => {
  const dispatch = useDispatch()
  const products = useSelector(state => state.admin.products)
  const update = useRef(false)
  const [current, setCurrent] = useState({ currentCategory: 'shirts', product: undefined })
  const [value, setValue] = useState('')
  const [showPopUp, setShowPopUp] = useState('')
  const [categories, setCategories] = useState([])

  useEffect(() => {
    if (current && current.currentCategory && !update.current) {
      dispatch({ type: ADMIN_PRODUCTS_GET, productType: current.currentCategory })
      update.current = true
    }
  }, [current, dispatch, products])

  useEffect(() => {
    const getCat = async () => {
      const res = await api.getCategories()
      return res.data
    }
    getCat().then(res => setCategories(res))
  },[])

  const addCat = async () => {
    const res = await api.addCategory(value)
    const cat = {
      id: '123' + getRandomId('3'),
      label: value,
      code: getRandomId('5')
    }
    setCategories([...categories, cat])
    setValue('')
    closePopUp()
  }

  const delCat = (e, catId) => {
    e.stopPropagation()
    console.log('catId', catId)
    setCategories([...categories.filter(cat => cat.id !== catId)])
  }

  const onCreateProduct = (product) => {
    dispatch({ type: ADMIN_PRODUCTS_PRODUCT_CREATE, product: product })
    closePopUp()
  }

  const onUpdateProduct = (product) => {
    dispatch({ type: ADMIN_PRODUCTS_PRODUCT_UPDATE, product: product })
    closePopUp()
  }

  const changeCurrentCategory = (category) => {
    console.log('changeCat', category)
    setCurrent({ ...current, currentCategory: category })
    update.current = false
  }

  const getBtnStyle = (tab) => classnames(style.btn, current.currentCategory === tab ? style.btn_active : '')

  const eachProduct = (product, i) => (
    <NeoWrapper key={i} className={style.product} onClick={() => setCurrent({ ...current, product: product })}>
      <div className={style.image_container}>
        <img src={product.image} alt={''} />
      </div>
      <div className={style.info}>
        <div className={style.box}><span className={style.subtitle}>Имя</span><span>{product.name}</span></div>
        <div className={style.box}>
          <div className={style.box}>
            <span className={style.subtitle}>Цена</span>
            <span>{product.price + ' ₽'}</span>
          </div>
          <div className={style.box}>
            <span className={style.subtitle}>Старая цена</span>
            <span>{product.priceOld + ' ₽'}</span>
          </div>
        </div>
        <div className={style.box}>
          <span className={style.subtitle}>Цвет</span>
          {product.colors && product.colors.map(color => <ButtonColor key={color} color={color} />)}

        </div>
        <div className={style.box}>
          <span className={style.subtitle}>Размер</span>
          <span>{product.size}</span>
        </div>
        <div className={style.box}>
          <span className={style.subtitle}>Колличество</span>
          <span>{product.count}</span>
        </div>
      </div>
      <div className={style.btn_box}>
        <ButtonPrimary onClick={() => editFile(product)}>Редактировать</ButtonPrimary>
        <ButtonPrimary onClick={() => editFile(product)}>Удалить</ButtonPrimary>
      </div>
      {/*{JSON.stringify(product)}*/}

    </NeoWrapper>
  )

  const editFile = (product) => {
    setCurrent({ ...current, product: product })
    setShowPopUp('product')
  }

  const showProducts = () => products && products[current.currentCategory] && products[current.currentCategory].map(eachProduct)

  const closePopUp = () => {
    setShowPopUp('')
    setCurrent({ ...current, product: undefined })
  }

  return (
    <div className={style.ap__products}>
      <div className={style.header}>
        {!!categories.length && categories.map(cat => (
          <ButtonPrimary key={cat.id} className={getBtnStyle(cat.code)} onClick={() => changeCurrentCategory(cat.id)}>
            {cat.label}
            <Delete className={style.delIcon} onClick={(e) => delCat(e, cat.id)} />
          </ButtonPrimary>
        ))}
        <ButtonPrimary className={style.btn} onClick={() => setShowPopUp('cat')}>
          Добавить категорию
        </ButtonPrimary>
        {/*<ButtonPrimary className={getBtnStyle('shirts')} onClick={() => changeCurrentCategory('shirts')}>*/}
        {/*  Рубашки*/}
        {/*</ButtonPrimary>*/}
        {/*<ButtonPrimary className={getBtnStyle('shoes')} onClick={() => changeCurrentCategory('shoes')}>*/}
        {/*  Обувь*/}
        {/*</ButtonPrimary>*/}
        {/*<ButtonPrimary className={getBtnStyle('accessories')} onClick={() => changeCurrentCategory('accessories')}>*/}
        {/*  Аксессуары*/}
        {/*</ButtonPrimary>*/}

      </div>
      <div className={style.header}>
        <ButtonPrimary className={style.btn} onClick={() => setShowPopUp('product')}>
          Создать продукт
        </ButtonPrimary>
      </div>
      <div className={style.products_list}>
        {showProducts()}
      </div>
      <PopUp show={showPopUp === 'product'} onClose={closePopUp} title={current.product ? 'Изменить продукт' : 'Создать продукт'}>
        <ProductForm
          product={current.product}
          closePopUp={closePopUp}
          onCreate={onCreateProduct}
          onUpdate={onUpdateProduct}
        />
      </PopUp>
      <PopUp show={showPopUp === 'cat'} onClose={closePopUp} title={'Создать категорию'}>
        <div>
          <Input value={value} onChange={e => setValue(e.target.value)} />
          <ButtonPrimary className={style.t_btn} onClick={addCat}>Создать</ButtonPrimary>
        </div>
      </PopUp>
    </div>
  )
}

export default Products
