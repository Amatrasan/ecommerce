import { useEffect, useState } from 'react'
import style from './Stocks.module.scss'
import { useDispatch, useSelector } from 'react-redux'
import { ADMIN_STOCKS_CREATE, ADMIN_STOCKS_DELETE, ADMIN_STOCKS_GET } from '../../../store/admin/stocks/actionTypes'
import ButtonPrimary from '../../UI/ButtonPrimary'
import { Delete } from '../../UI/Icons'
import PopUp from '../../UI/PopUp'

const Stocks = () => {
  const stocks = useSelector(state => state.admin.stocks)
  const [showPopUp, setShowPopUp] = useState('')
  const [currentStock, setCurrentStock] = useState()
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch({ type: ADMIN_STOCKS_GET })
  }, [dispatch])

  const handleFileChange = (e) => {
    const file = e.target.files ? e.target.files[0] : e.dataTransfer.files[0]
    const fr = new FileReader()
    fr.onload = function () {
      const result = ({ result: fr.result, file: file })
      dispatch({ type: ADMIN_STOCKS_CREATE, stock: result })
    }
    fr.readAsDataURL(file)
  }

  const deleteStock = () => {
    dispatch({ type: ADMIN_STOCKS_DELETE, stockId: currentStock.id })
    closePopUp()
  }

  const eachStock = (stock, i) => (
    <div className={style.stock} key={i} onClick={() => setCurrentStock(stock)}>
      <img className={style.img} src={stock.img} alt={''}/>
      <ButtonPrimary className={style.btn} onClick={() => setShowPopUp('DELETE')}>
        <Delete />
      </ButtonPrimary>
    </div>
  )

  const showStocks = () => stocks && stocks.map(eachStock)

  const closePopUp = () => setShowPopUp('')

  return (
    <div className={style.ap__stocks}>
      <div className={style.header}>
        <input type={'file'} id={'stocks_input_file'} onChange={handleFileChange} accept={'image/png,image/jpeg,image/jpg'}/>
        <ButtonPrimary
          className={style.btn}
          onClick={() => document.querySelector('#stocks_input_file').click()}
        >
          Добавить новую акцию
        </ButtonPrimary>
      </div>
      <div className={style.stocks__list}>
        {showStocks()}
      </div>
      <PopUp show={showPopUp === 'DELETE'} onClose={closePopUp} title={'Удалить акцию'}>
        <div>Удалить выбранную акцию?</div>
        <div>
          <ButtonPrimary onClick={deleteStock}>Да</ButtonPrimary>
          <ButtonPrimary onClick={closePopUp}>Нет</ButtonPrimary>
        </div>
      </PopUp>
    </div>
  )
}

export default Stocks
