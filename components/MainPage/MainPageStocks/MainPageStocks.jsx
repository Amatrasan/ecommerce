import { useRef } from 'react'
import style from './MainPageStocks.module.scss'
import Slider from '../../UI/Slider'
import NeoWrapper from '../../UI/NeoWrapper'
import ButtonPrimary from '../../UI/ButtonPrimary'
import { Arrow, ArrowEasy } from '../../UI/Icons'

const MainPageStocks = (props) => {
  const slider = useRef({})

  const showStocks = () => props.stocks && props.stocks.map((stock, i) => (
    <div className={style.img_container} key={i}>
      <img src={stock.img} alt={''} loading={'lazy'} />
    </div>
  ))
  return (
    <NeoWrapper className={style.stock_slider}>
      <div className={style.arrow_container}>
        <ButtonPrimary className={style.arrow} onClick={() => slider.current.slickPrev()}>
          <ArrowEasy/>
        </ButtonPrimary>
        <ButtonPrimary className={style.arrow} onClick={() => slider.current.slickNext()}>
          <ArrowEasy direction={'right'}/>
        </ButtonPrimary>
      </div>
      <Slider slidesToShow={1} swipeToSlide={true} arrows={false} ref={slider}>
        {showStocks()}
      </Slider>

    </NeoWrapper>

  )
}

export default MainPageStocks
