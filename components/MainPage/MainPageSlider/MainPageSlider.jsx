import { useRef } from 'react'
import style from './MainPageSlider.module.scss'
import PropTypes from 'prop-types'
import ProductCard from '../../UI/ProductCard'
import Slider from '../../UI/Slider'
import classnames from '../../../utils/classnames'
import { ArrowEasy } from '../../UI/Icons'
import img from './../../../public/mockups/a1.webp'
import ButtonPrimary from '../../UI/ButtonPrimary'

const MainPageSlider = (props) => {
  const slider = useRef({})

  const settings = {
    dots: false,
    arrows: false,
    lazyLoad: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    initialSlide: 1,
    swipeToSlide: true,
    responsive: [
      {
        breakpoint: 1440,
        settings: {
          slidesToShow: 3,
        }
      },
    ]
  }

  const showProducts = () => props.products && props.products.map((product, i) => <ProductCard key={i} product={product} />)

  return (
    <div className={classnames(style.app__slider_new_products, style.slider_new_products)}>
      <div className={classnames(style.slider_new_products__header, style.header)}>
        <div className={style.title}>{props.title || 'Title'}</div>
        <div className={classnames(style.header__navigation_box, style.navigation_box)}>
          <ButtonPrimary className={style.arrow} onClick={() => slider.current.slickPrev()}>
            <ArrowEasy />
          </ButtonPrimary>
          <ButtonPrimary className={style.arrow} onClick={() => slider.current.slickNext()}>
            <ArrowEasy direction={'right'} />
          </ButtonPrimary>
        </div>
      </div>
      <Slider { ...settings } ref={slider} className={style.slider_new_products__slider}>
        {showProducts()}
      </Slider>

    </div>
  )
}

MainPageSlider.propTypes = {
  title: PropTypes.string
}

export default MainPageSlider
