
import MainPageStocks from './MainPageStocks'
import MainPageSlider from './MainPageSlider'
import style from './MainPage.module.scss'

import { useSelector } from 'react-redux'

const MainPage = () => {
  const products = useSelector(state => state.app.products)
  const stocks = useSelector(state => state.app.stocks)

  return (
    <>
      <MainPageStocks stocks={stocks} />
      <MainPageSlider title={'Новинки'} products={products.news || []} />
      <div className={style.main_page__bottom_slider}>
        <MainPageSlider title={'Хиты'} products={products.hits || []} />
      </div>
    </>
  )
}

export default MainPage
