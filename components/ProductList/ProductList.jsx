import style from './ProductList.module.scss'
import PropTypes from 'prop-types'
import ProductCard from '../UI/ProductCard'

const ProductList = ({ products }) => {
  const showProducts = () => products && products.map((product, i) => <ProductCard product={product} key={i}/>)

  return (
    <div className={style.product_list}>
      {showProducts()}
    </div>
  )
}

ProductList.propTypes = {
  products: PropTypes.arrayOf(PropTypes.object).isRequired
}

export default ProductList
