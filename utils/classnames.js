export default function classnames () {
  let result = []

  for (let i = 0; i < arguments.length; i++) {
    if (arguments[i]) {
      result.push(arguments[i])
    }
  }
  return result.join(' ')
}
