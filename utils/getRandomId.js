const getRandomArbitrary = (min, max) => {
  return Math.floor(Math.random() * (max - min) + min)
}

const getRandomId = (length = 5) => {
  const words = 'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM'
  let id = ''
  for (let i = 0; i <= length; i++) {
    id += words[getRandomArbitrary(0, words.length - 1)]
  }
  return id
}

export default getRandomId
