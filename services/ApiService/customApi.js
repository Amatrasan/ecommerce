import Axios from 'axios'
import 'regenerator-runtime/runtime'

class Queue {
  _store = [];
  push (val) {
    this._store.push(val)
  }
  pop () {
    return this._store.shift()
  }
  isEmpty () {
    return this._store.length === 0
  }
}

class ApiSetting {
  /**
   * @param {{ silentRequest: boolean, ...axiosConfig: object}} axiosConfig - axios configuration
   * @param {{ onAccessTokenExpired: function, onRequestStart: function, onRequestFinish: function, endpoints: { auth: object, public: object | string }, tokenType: string }} apiConfig
   * @param {{ getTokens: function, setTokens: function, clearTokens: function }} apiTokensStorage
   */
  constructor (axiosConfig, apiConfig, apiTokensStorage) {
    if (!(apiTokensStorage.getTokens && apiTokensStorage.setTokens && apiTokensStorage.clearTokens)) {
      throw new Error('apiTokensStorage must have required methods: getTokens, setTokens and clearTokens')
    }
    if (!(apiConfig.endpoints && apiConfig.endpoints.auth && apiConfig.endpoints.public)) {
      throw new Error('apiConfig must have required fields in the endpoints: auth, public')
    }
    this.tokenStorage = apiTokensStorage
    this.apiConfig = apiConfig
    this.axios = new Axios.create(axiosConfig)
    this.isTokenRefreshing = false
    this.requestQueue = new Queue()
    this.axios.interceptors.request.use(config => {
      const tokens = this.tokenStorage.getTokens()
      const isPublic = this.isPublicUrl(config)
      config.headers['Access-Control-Allow-Origin'] = '*'
      if (!isPublic && this.isTokensExpired(tokens)) {
        if (!tokens.refreshToken) {
          this.handleAccessTokenExpired()
          return Promise.reject(new Error('Token expired'))
        }

        if (!this.isTokenRefreshing) {
          this.refreshTokens(tokens.refreshToken)
        }

        this.handleRequestStart(config)
        return new Promise((resolve, reject) => {
          this.requestQueue.push({
            config,
            resolve,
            reject,
          })
        })
      } else if (!isPublic) {
        this.setAuthorizationHeader(config)
      }
      this.handleRequestStart(config)
      return config
    },
    error => {
      this.handleRequestFinish(error.config)

      return Promise.reject(error)
    })
    this.axios.interceptors.response.use((response) => {
      this.handleRequestFinish(response.config)
      return response
    },(error) => {
      const {
        config,
        response
      } = error
      const tokens = this.tokenStorage.getTokens()

      this.handleRequestFinish(config)

      if (response && response.status === 401) {
        if (this.isRefreshUrl(config)) {
          this.tokenStorage.clearTokens()
          return Promise.reject(error)
        }
        if (!this.isTokensValid(tokens) && !this.isPublicUrl(config)) {
          if (!this.isTokenRefreshing) {
            this.refreshTokens(tokens.refreshToken)
          }
          return new Promise((resolve, reject) => {
            this.requestQueue.push({
              config,
              resolve: (config) => resolve(this.axios(config)),
              reject,
            })
          })
        }
      }
      return Promise.reject(error)
    })
  }

  handleAccessTokenExpired () {
    if (this.apiConfig.onAccessTokenExpired) {
      this.apiConfig.onAccessTokenExpired()
    }
  }


  isTokensExpired (tokens) {
    const expires = tokens.expired_at || 0
    return Date.now() >= expires
  }

  isPublicUrl (config) {
    const isAuthApi = () => {
      return Object.values(this.apiConfig.endpoints.auth).some(url => config.url.match(url))
    }

    if (this.apiConfig.endpoints.public && this.apiConfig.endpoints.auth) {
      let flagPublic
      if (this.apiConfig.endpoints.public instanceof RegExp) {
        const regExp = new RegExp(this.apiConfig.endpoints.public)
        flagPublic = regExp.test(config.url)
      } else {
        flagPublic = config.url.match(this.apiConfig.endpoints.public)
      }
      return flagPublic || isAuthApi()
    } else {
      return true
    }
  }

  handleRequestStart = (config = {}) => {
    if (this.apiConfig.onRequestStart && !config.silentRequest) {
      this.apiConfig.onRequestStart()
    }
  }

  handleRequestFinish = (config = {}) => {
    if (this.apiConfig.onRequestFinish && !config.silentRequest) {
      this.apiConfig.onRequestFinish()
    }
  }

  setAuthorizationHeader (config) {
    const { accessToken } = this.tokenStorage.getTokens()
    let authorizationHeader = this.apiConfig.tokenType ? this.apiConfig.tokenType + ' ' : ''
    if (accessToken) {
      config.headers.Authorization = authorizationHeader + accessToken
    }
  }

  isRefreshUrl (config) {
    return config.url.match(this.apiConfig.endpoints.auth.refresh)
  }

  isTokensValid (tokens) {
    return tokens.refreshToken && !this.isTokensExpired(tokens)
  }

  setTokensData (token) {
    this.tokenStorage.setTokens({ refreshToken: this.tokenStorage.getTokens().refreshToken, accessToken: token })
  }

  onRefreshTokensSuccess () {

    while (!this.requestQueue.isEmpty()) {
      const r = this.requestQueue.pop()
      if (!r) return
      this.setAuthorizationHeader(r.config)
      r.resolve(r.config)
    }
  }
  onRefreshTokensFail (error) {

    while (!this.requestQueue.isEmpty()) {
      const r = this.requestQueue.pop()
      if (!r) return
      r.reject(error)
    }
    this.tokenStorage.clearTokens()
  }

  async refreshTokens (refreshToken) {
    this.isTokenRefreshing = true
    this.axios.post(this.apiConfig.endpoints.auth.refresh, {
      token: refreshToken
    }).then(response => {
      this.setTokensData(response.data.accessToken)
      this.isTokenRefreshing = false
      this.onRefreshTokensSuccess()
    })
      .catch(error => {
        this.isTokenRefreshing = false
        this.onRefreshTokensFail(error)
      })
  }

  put = (URL, payload, options) => {
    return this.axios.put(URL, payload, options)
  }

  get = (URL, config) => {
    return this.axios.get(URL, config)
  }

  post = (URL, payload, config) => {
    return this.axios.post(URL, payload, config)
  }

  delete = (URL, options) => {
    return this.axios.delete(URL, options)
  }

  request = (config) => {
    return this.axios.request(config)
  }
}

export default ApiSetting
