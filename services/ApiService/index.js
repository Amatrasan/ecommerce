import Api from './customApi'
import { store } from './../StoreService/index'
import { APP_AUTH_TOKEN_CLEAR, APP_AUTH_LOGIN_GET } from '../../store/auth/actionTypes'

const isServer = typeof window === 'undefined'
const PROD_URL = ('https://' + 'ecommerce-five-eta.vercel.app' + '/api')
const DEV_URL = !isServer ? (window.location.origin + '/api') : 'http://localhost:3000/api'
const isProd = !isServer ? !!window.location.origin.match('localhost') : 'http://localhost:3000/api'


const axiosConfig = {
  baseURL: DEV_URL,
  timeout: 300 * 1000,
  silentRequest: false
}

const apiConfig = {
  onAccessTokenExpired: () => store.dispatch({ type: APP_AUTH_TOKEN_CLEAR }),
  endpoints: {
    auth: {
      login: '/auth/login',
      refresh: '/auth/refresh',
      register: '/auth/register'
    },
    public: /products|product|stocks/gm
  },
  tokenType: 'Bearer '
}

const getTokens = () => {
  if (typeof window !== 'undefined') {
    return localStorage['token'] ? JSON.parse(localStorage['token']) : {}
  } else {
    return {}
  }
}

const setTokens = (token) => {
  if (typeof window !== 'undefined') {
    localStorage.setItem('token', JSON.stringify(token))
    store.dispatch({ type: APP_AUTH_LOGIN_GET, payload: token })
  }
}

const clearTokens = () => {
  if (typeof window !== 'undefined') {
    localStorage.removeItem('token')
    store.dispatch({ type: APP_AUTH_TOKEN_CLEAR })
  }
}

const tokenStorage = {
  getTokens: getTokens,
  setTokens: setTokens,
  clearTokens: clearTokens,
}

class ApiPublicService {
  constructor () {
    this.api = new Api(axiosConfig, apiConfig, tokenStorage)
  }

  // common

  getProducts (params = {}) {
    // console.log('params', params)
    return this.api.get('/product', { params: params })
  }

  getProduct (productId) {
    console.log('API CALL', productId)
    return this.api.get(`/product/${productId}`, { params: { productId } })
  }

  getStocks () {
    return this.api.get('/stocks')
  }

  login (credentials) {
    return this.api.post(apiConfig.endpoints.auth.login, credentials)
  }

  register (credentials) {
    return this.api.post(apiConfig.endpoints.auth.register, credentials)
  }


  // user

  getUser () {
    return  this.api.get('/user')
  }

  updateUser (user) {
    return this.api.put('/user', { user })
  }

  deleteCardUser () {
    return this.api.get('/user')
  }

  createCardUser () {
    return this.api.post('/user')
  }

  // basket

  getBaskets () {
    return this.api.get('/add_to_basket')
  }

  getBasketProducts (basketId) {
    return this.api.get('/add_to_basket', { basketId })
  }

  deleteBasket () {
    return this.api.delete('/add_to_basket')
  }

  deleteProductInBasket (basketId, productId) {
    return this.api.delete('/add_to_basket', { basketId, productId })
  }

  addHumanInBasket (userId, basketId) {
    return this.api.post('/add_to_basket', { userId: userId, basketId: basketId })
  }

  deleteHumanInBasket (userId, basketId) {
    return this.api.delete('/add_to_basket', { userId: userId, basketId: basketId })
  }

  buyBasket (basketId) {
    return this.api.delete('/add_to_basket', { basketId: basketId })
  }

  createBasket (name) {
    return this.api.post('/add_to_basket', { name })
  }

  addToBasket (productId, basketId, options) {
    return this.api.post('/add_to_basket', { productId, basketId, options })
  }







  // admin

  createAdminStock (stock) {
    return this.api.post('/admin/stock', stock)
  }

  deleteAdminStock (stockId) {
    return this.api.delete('/admin/stock', { stockId })
  }

  getAdminProducts (params = { admin: true }) {
    return this.api.get('/product?admin=true', params)
  }

  createAdminProduct (product) {
    return this.api.post('/admin/product', product)
  }

  updateAdminProduct (product) {
    return this.api.put('/admin/product', product)
  }

  getCategories () {
    return this.api.get('/category')
  }

  addCategory (name) {
    return this.api.post('/category', { name })
  }
}

export default new ApiPublicService()
