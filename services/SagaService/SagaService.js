import { all, fork } from 'redux-saga/effects'
import { productsWatcher } from '../../store/app/products/productsShoes/sagas'
import { stocksWatcher } from '../../store/app/stocks/sagas'
import { authWatcher } from '../../store/auth/sagas'
import { productWatcher } from '../../store/app/products/product/sagas'
import { userWatcher } from '../../store/user/sagas'
import { basketsWatcher } from '../../store/app/baskets/sagas'
import { adminProductsWatcher } from '../../store/admin/products/sagas'
import { adminStocksWatcher } from '../../store/admin/stocks/sagas'


export default function* rootSaga () {
  yield fork(productsWatcher)
  yield fork(productWatcher)
  yield fork(stocksWatcher)
  yield fork(authWatcher)
  yield fork(userWatcher)
  yield fork(basketsWatcher)
  yield fork(adminProductsWatcher)
  yield fork(adminStocksWatcher)
}

// export default function* rootSaga () {
//   yield all([
//     productsWatcher(),
//     productWatcher(),
//     stocksWatcher(),
//     authWatcher(),
//     userWatcher(),
//     basketsWatcher(),
//     adminProductsWatcher(),
//     adminStocksWatcher()
//   ])
// }
