import { createStore, applyMiddleware } from 'redux'
import { createWrapper } from 'next-redux-wrapper'
import createSagaMiddleware from 'redux-saga'
import { createMigrate, persistStore } from 'redux-persist'
import { composeWithDevTools } from 'redux-devtools-extension'
import migrations from './migrate'
import rootSaga from './../SagaService/SagaService'
import rootReducer from '../../store'

const makeStore = () => {
  let store

  const sagaMiddleware = createSagaMiddleware()

  const isClient = typeof window !== 'undefined'

  if (isClient) {
    const { persistReducer } = require('redux-persist')
    const storage = require('redux-persist/lib/storage').default

    const persistConfig = {
      key: 'root',
      version: 3,
      storage,
      migrate: createMigrate(migrations, { debug: false }),
    }

    const makeConfiguredStore = (reducer) => createStore(
      reducer(persistConfig, rootReducer),
      composeWithDevTools(applyMiddleware(sagaMiddleware))
    )

    store = makeConfiguredStore(persistReducer)

    store.__PERSISTOR = persistStore(store)
  } else {
    store = createStore(
      rootReducer,
      composeWithDevTools(applyMiddleware(sagaMiddleware))
    )
  }

  store.sagaTask = sagaMiddleware.run(rootSaga)

  return store
}

export const store = makeStore()

const wrapper = createWrapper(makeStore)

export default wrapper
