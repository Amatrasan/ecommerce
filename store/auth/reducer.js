import * as types from './actionTypes'
const initialState = null

const auth = (state = initialState, { type, payload }) => {
  switch (type) {
  case types.APP_AUTH_LOGIN_GET:
    return {
      ...state,
      message: '',
      token: payload
    }
  case types.APP_AUTH_LOGIN_GET_ERROR:
    return {
      ...state,
      message: payload
    }
  case types.APP_AUTH_LOGOUT:
  case types.APP_AUTH_TOKEN_CLEAR:
    return initialState
  default:
    return state
  }
}

export default auth
