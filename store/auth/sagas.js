import { put, take, all, call } from 'redux-saga/effects'
import api from '../../services/ApiService'
import * as types from './actionTypes'
import { toast } from 'react-toastify'

function* authLogin () {
  while (true) {
    const { email, password } = yield take(types.APP_AUTH_LOGIN_REQUEST)
    try {
      const result = yield api.login({ email, password })
      if (result.data.token) {
        if (typeof window !== 'undefined') {
          localStorage.setItem('token', JSON.stringify(result.data.token))
        }
        yield put({ type: types.APP_AUTH_LOGIN_GET, payload: result.data.token })
      } else {
        yield call(() => toast.error(result.data.message || 'Что-то пошло не так'))
      }
    } catch (err) {
      yield call(() => toast.error(err.message || 'Что-то пошло не так'))
    }
  }
}

function* authRegister () {
  while (true) {
    const { credentials, callback } = yield take(types.APP_AUTH_REGISTER_REQUEST)
    try {
      const result = yield api.register(credentials)
      if (result.data.token) {
        if (typeof window !== 'undefined') {
          localStorage.setItem('token', JSON.stringify(result.data.token))
        }
        yield put({ type: types.APP_AUTH_LOGIN_GET, payload: result.data.token })
        if (callback) {
          console.log('SAGA', callback)
          yield call(callback, true)
        }
      } else {
        yield call(() => toast.error(result.data.message || 'Что-то пошло не так'))
      }
    } catch (err) {
      yield call(() => toast.error(err.message || 'Что-то пошло не так'))
    }
  }
}

function* authLogOut () {
  while (true) {
    const result = yield take(types.APP_AUTH_LOGOUT)
    yield put({ type: types.APP_AUTH_TOKEN_CLEAR })
  }
}

export function* authWatcher () {
  yield all([
    authLogin(),
    authLogOut(),
    authRegister()
  ])
}
