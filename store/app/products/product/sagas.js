import * as types from './actionTypes'
import api from '../../../../services/ApiService'
import { put, fork, take, call } from 'redux-saga/effects'
// import { take } from '@redux-saga/core/effects'


export function fetchProductSuccess (payload) {
  return {
    type: types.APP_PRODUCTS_PRODUCT_GET_SUCCESS,
    payload: payload
  }
}

function* fetchProduct (id) {
  const result = yield api.getProduct(id)
  yield put(fetchProductSuccess(result.data))
}

export function* productWatcher () {
  while (true) {
    const { id } = yield take(types.APP_PRODUCTS_PRODUCT_GET)
    const result = yield call(api.getProduct, id)
    yield put(fetchProductSuccess(result.data))
  }
}
