import * as types from './actionTypes'
const initialState = {}

const product = (state = initialState, { type, payload })=> {
  switch (type) {
  case types.APP_PRODUCTS_PRODUCT_GET_SUCCESS:
    return { ...state, [payload.id]: payload }
  default:
    return state
  }
}

export default product
