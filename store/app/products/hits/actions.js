import * as types from './actionTypes'

export function fetchProductsHitsSuccess (payload) {
  return {
    type: types.APP_PRODUCTS_PRODUCTS_HITS_GET_SUCCESS,
    payload: payload
  }
}
