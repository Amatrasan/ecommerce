import * as types from './actionTypes'
const initialState = []

const productsHits = (state = initialState, { type, payload })=> {
  switch (type) {
  case types.APP_PRODUCTS_PRODUCTS_HITS_GET_SUCCESS:
    return payload
  default:
    return state
  }
}

export default productsHits
