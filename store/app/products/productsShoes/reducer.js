import * as types from './actionTypes'
const initialState = []

const products = (state = initialState, { type, payload })=> {
  switch (type) {
  case types.APP_PRODUCTS_PRODUCTS_SHOES_GET_SUCCESS:
    return payload
  default:
    return state
  }
}

export default products
