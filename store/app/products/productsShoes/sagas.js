import api from '../../../../services/ApiService'
import * as types from './actionTypes'
import { put, takeLatest } from 'redux-saga/effects'

function* fetchProducts () {
  const result = yield api.getProducts()
  yield put({ type: types.APP_PRODUCTS_PRODUCTS_SHOES_GET_SUCCESS, payload: result.data, })
}

export function* productsWatcher () {
  yield takeLatest(types.APP_PRODUCTS_PRODUCTS_SHOES_GET, fetchProducts)
}
