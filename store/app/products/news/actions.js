import * as types from './actionTypes'

export function fetchProductsNewsSuccess (payload) {
  return {
    type: types.APP_PRODUCTS_PRODUCTS_NEWS_GET_SUCCESS,
    payload: payload
  }
}
