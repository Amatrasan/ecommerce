import * as types from './actionTypes'
const initialState = []

const productsNews = (state = initialState, { type, payload })=> {
  switch (type) {
  case types.APP_PRODUCTS_PRODUCTS_NEWS_GET_SUCCESS:
    return payload
  default:
    return state
  }
}

export default productsNews
