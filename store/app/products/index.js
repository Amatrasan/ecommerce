import { combineReducers } from 'redux'
import hits from './hits/reducer'
import productShoes from './productsShoes/reducer'
import news from './news/reducer'
import product from './product/reducer'

const productReducer = combineReducers({
  hits,
  news,
  productShoes,
  product
})
const storage = require('redux-persist/lib/storage').default

const persistConfig = {
  key: 'product',
  version: 3,
  whitelist: ['hits', 'news','productShoes'],
  storage
}

const { persistReducer } = require('redux-persist')
const reducer = persistReducer(persistConfig, productReducer)

export default reducer
