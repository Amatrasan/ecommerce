import * as types from './actionTypes'
const initialState = ''

const colorTheme = (state = initialState, { type, payload })=> {
  switch (type) {
  case types.CHANGE_THEME:
    return payload
  default:
    return state
  }
}

export default colorTheme
