import * as types from './actionTypes'

export function changeThemeSuccess (payload) {
  return {
    type: types.CHANGE_THEME,
    payload
  }
}
