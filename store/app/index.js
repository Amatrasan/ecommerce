import { combineReducers } from 'redux'
import colorTheme from './colorTheme/reducer'
import products from './products/index'
import stocks from './stocks/reducer'
import baskets from './baskets/reducer'

const appReducer = combineReducers({
  colorTheme,
  products,
  stocks,
  baskets
})

export default appReducer
