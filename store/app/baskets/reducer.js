import * as types from './actionTypes'
import { APP_AUTH_LOGOUT } from '../../auth/actionTypes'
const initialState = [
  { id: '1', name: 'test', main: true, users: [{ id: 123, firstName: 'Геннадий', lastName: 'Строков', patronymic: 'Сергеевич' }], products: [] },
  { id: '2', name: 'test', users: [{ id: 123, firstName: 'Иван', lastName: 'Иванов', patronymic: 'Иванович' }], products: [] },
  { id: '2', name: 'test', owner: true, users: [{ id: 123, firstName: 'Иван', lastName: 'Иванов', patronymic: 'Иванович' }], products: [] },
  { id: '2', name: 'test', users: [{ id: 123, firstName: 'Иван', lastName: 'Иванов', patronymic: 'Иванович' }], products: [] }
]

const baskets = (state = initialState, { type, payload })=> {
  switch (type) {
  case APP_AUTH_LOGOUT:
    return initialState
  case types.APP_BASKETS_GET_SUCCESS:
    return [...payload]
  case types.APP_BASKETS_DELETE_BASKET_SUCCESS:
    return [...state.filter(basket => basket.id !== payload.basketId)]
  case types.APP_BASKETS_CREATE_BASKET_SUCCESS:
    return [ payload, ...state ]
  case types.APP_BASKETS_GET_BASKET_PRODUCTS_SUCCESS:
    return [...state.map(basket => basket.id === payload.basketId ? { ...basket, products: payload.products } : basket)]
  case types.APP_BASKETS_ADD_PRODUCT_SUCCESS:
    return [...state.map(basket => basket.id === payload.basketId ? { ...basket, products: basket.products ? [ payload.product, ...basket.products ] : [ payload.product ] } : basket)]
  case types.APP_BASKET_DELETE_PRODUCT_IN_BASKET_SUCCESS:
    return [...state.map(basket => basket.id === payload.basketId ? { ...basket, products: basket.products.filter(product => product.id !== payload.productId) } : basket)]
  case types.APP_BASKET_ADD_HUMAN_IN_BASKET_SUCCESS:
    return [...state.map(basket => basket.id === payload.basketId ? { ...basket, users: basket.users ? [ ...basket.users, payload.user ] : [payload.user] } : basket)]
  case types.APP_BASKET_DELETE_HUMAN_IN_BASKET_SUCCESS:
    return [...state.map(basket => basket.id === payload.basketId ? { ...basket, users: basket.users.filter(user => user.id !== payload.userId) } : basket)]
  case types.APP_BASKET_BUY_SUCCESS:
    return [...state.map(basket => basket.id === payload.basketId ? { ...basket, products: [] } : basket)]
  default:
    return state
  }
}

export default baskets
