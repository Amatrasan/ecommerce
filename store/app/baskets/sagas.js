import { all, put, take, call } from 'redux-saga/effects'
import * as types from './actionTypes'
import api from '../../../services/ApiService'
import { toast } from 'react-toastify'

const resultBaskets = [
  { id: '1', name: 'Моя корзина', main: true, users: [{ id: 123, firstName: 'Геннадий', lastName: 'Строков', patronymic: 'Сергеевич' }], products: [] },
  { id: '2', name: 'Какая то корзина', users: [      {
    id: 123141,
    firstName: 'Петя',
    lastName: 'Петрин',
    patronymic: 'Петрович'
  },
  {
    id: 1241,
    firstName: 'Иван',
    lastName: 'Иванович',
    patronymic: 'Иванов'
  }], products: [] },
  { id: '3', name: 'test', owner: true, users: [{ id: 123, firstName: 'Иван', lastName: 'Иванов', patronymic: 'Иванович' }], products: [] },
  { id: '4', name: 'test2', users: [{ id: 123, firstName: 'Иван', lastName: 'Иванов', patronymic: 'Иванович' }], products: [] },
  // {
  //   id: 1,
  //   isAdmin: true,
  //   name: 'Моя корзина',
  //   main: true,
  // },
  // {
  //   id: 1234,
  //   isAdmin: false,
  //   name: 'Какая то корзина',
  //   users: [
  //     {
  //       id: 123141,
  //       firstName: 'Петя',
  //       lastName: 'Петрин',
  //       patronymic: 'Петрович'
  //     },
  //     {
  //       id: 1241,
  //       firstName: 'Иван',
  //       lastName: 'Иванович',
  //       patronymic: 'Иванов'
  //     }
  //   ]
  // }
]

const products = [
  {
    id: '12fa',
    image: '/_next/static/images/a1-bcc3a0e535bb220959a4ed70fca7e0e9.webp',
    name: 'Продукт',
    category: 'shoes',
    size: 'M',
    color: 'blue',
    price: 123.12,
    unit: 'Р'
  },
  {
    id: '12faasw',
    image: '/_next/static/images/a1-bcc3a0e535bb220959a4ed70fca7e0e9.webp',
    name: 'Продукт2',
    category: 'shoes',
    size: 'M',
    color: 'blue',
    price: 123.12,
    unit: 'Р',
    customer: true,
  }
]

const basket = (name) => {
  return {
    id: 1,
    isAdmin: true,
    name: name,
    humans: [],
    products: []
  }
}


function* getBaskets () {
  while (true) {
    yield take(types.APP_BASKETS_GET)
    const result = yield api.getBaskets()
    yield put({ type: types.APP_BASKETS_GET_SUCCESS, payload: resultBaskets, })
  }
}

function* createBasket () {
  while (true) {
    const { name } = yield take(types.APP_BASKETS_CREATE_BASKET)
    const result = yield api.createBasket()
    yield put({ type: types.APP_BASKETS_CREATE_BASKET_SUCCESS, payload: basket(name), })
    yield call(getToastMessage, `Корзина ${name} создана`)
  }
}

function* deleteBasket () {
  while (true) {
    const { basketId } = yield take(types.APP_BASKETS_DELETE_BASKET)
    const result = yield api.deleteBasket()
    yield put({ type: types.APP_BASKETS_DELETE_BASKET_SUCCESS, payload: { basketId }, })
    yield call(getToastMessage, 'Корзина удалена')
  }
}

function* getBasketProducts () {
  while (true) {
    const { basketId } = yield take(types.APP_BASKETS_GET_BASKET_PRODUCTS)
    const result = yield api.getBasketProducts(basketId)
    yield put({ type: types.APP_BASKETS_GET_BASKET_PRODUCTS_SUCCESS, payload: { basketId, products: products }, })
  }
}


function* addToBasket () {
  while (true) {
    const { product, basketId, options } = yield take(types.APP_BASKETS_ADD_PRODUCT)
    const result = yield api.addToBasket(product, basketId, options)
    const productInBasket = {
      id: product.id,
      image: product.image,
      name: product.name,
      size: options.size,
      color: options.color,
      price: product.price
    }
    yield put({ type: types.APP_BASKETS_ADD_PRODUCT_SUCCESS, payload: { basketId: basketId, product: productInBasket } })
  }
}

function* deleteProductInBasket () {
  while (true) {
    const { basketId, productId } = yield take(types.APP_BASKET_DELETE_PRODUCT_IN_BASKET)
    const result = yield api.deleteProductInBasket(basketId, productId)
    yield put({ type: types.APP_BASKET_DELETE_PRODUCT_IN_BASKET_SUCCESS, payload: { basketId, productId }, })
  }
}

function getToastMessage (message, type = '') {
  if (type === 'error') {
    toast.error(message || 'Что-то пошло не так')
  } else {
    toast.success(message || 'Успешно')
  }
}

function* addHumanInBasket () {
  while (true) {
    const { basketId, userId } = yield take(types.APP_BASKET_ADD_HUMAN_IN_BASKET)
    const result = yield api.addHumanInBasket(basketId, userId)
    const mock = {
      id: userId,
      firstName: 'Иван',
      lastName: 'Иванович',
      patronymic: 'Иванов'
    }
    if (result.data.human) {
      yield call(getToastMessage, ('Пользователь' + firstName + 'приглашен в корзину'), 'error')
      yield put({ type: types.APP_BASKET_ADD_HUMAN_IN_BASKET_SUCCESS, payload: { basketId, user: mock }, })
    } else {
      yield call(getToastMessage, result.data.message, 'error')
    }
  }
}

function* deleteHumanInBasket () {
  while (true) {
    const { basketId, userId } = yield take(types.APP_BASKET_DELETE_HUMAN_IN_BASKET)
    const result = yield api.deleteHumanInBasket(userId, basketId)
    if (!result.data.message) {
      yield put({ type: types.APP_BASKET_DELETE_HUMAN_IN_BASKET_SUCCESS, payload: { basketId, userId: userId }, })
    } else {
      yield call(getToastMessage, result.data.message, 'error')
    }
  }
}

function* buyBasket () {
  while (true) {
    const { basketId } = yield take(types.APP_BASKET_BUY)
    const result = yield api.buyBasket(basketId)
    if (!result.data.message) {
      yield put({ type: types.APP_BASKET_BUY_SUCCESS, payload: { basketId: basketId } })
      yield call(getToastMessage)
    } else {
      yield call(getToastMessage, result.data.message)
    }
  }
}

// APP_BASKET_DELETE_HUMAN_IN_BASKET

export function* basketsWatcher () {
  yield all([
    getBaskets(),
    createBasket(),
    deleteBasket(),
    getBasketProducts(),
    addToBasket(),
    deleteProductInBasket(),
    addHumanInBasket(),
    deleteHumanInBasket(),
    buyBasket()
  ])
}
