export const APP_BASKETS_GET = 'APP_BASKETS_GET'
export const APP_BASKETS_GET_SUCCESS = 'APP_BASKETS_GET_SUCCESS'

export const APP_BASKETS_GET_BASKET_PRODUCTS = 'APP_BASKETS_GET_BASKET_PRODUCTS'
export const APP_BASKETS_GET_BASKET_PRODUCTS_SUCCESS = 'APP_BASKETS_GET_BASKET_PRODUCTS_SUCCESS'

export const APP_BASKETS_DELETE_BASKET = 'APP_BASKETS_DELETE_BASKET'
export const APP_BASKETS_DELETE_BASKET_SUCCESS = 'APP_BASKETS_DELETE_BASKET_SUCCESS'

export const APP_BASKETS_CREATE_BASKET = 'APP_BASKETS_CREATE_BASKET'
export const APP_BASKETS_CREATE_BASKET_SUCCESS = 'APP_BASKETS_CREATE_BASKET_SUCCESS'

export const APP_BASKETS_ADD_PRODUCT = 'APP_BASKETS_ADD_PRODUCT'
export const APP_BASKETS_ADD_PRODUCT_SUCCESS = 'APP_BASKETS_ADD_PRODUCT_SUCCESS'

export const APP_BASKET_DELETE_PRODUCT_IN_BASKET = 'APP_BASKET_DELETE_PRODUCT_IN_BASKET'
export const APP_BASKET_DELETE_PRODUCT_IN_BASKET_SUCCESS = 'APP_BASKET_DELETE_PRODUCT_IN_BASKET_SUCCESS'

export const APP_BASKET_ADD_HUMAN_IN_BASKET = 'APP_BASKET_ADD_HUMAN_IN_BASKET'
export const APP_BASKET_ADD_HUMAN_IN_BASKET_SUCCESS = 'APP_BASKET_ADD_HUMAN_IN_BASKET_SUCCESS'

export const APP_BASKET_DELETE_HUMAN_IN_BASKET = 'APP_BASKET_DELETE_HUMAN_IN_BASKET'
export const APP_BASKET_DELETE_HUMAN_IN_BASKET_SUCCESS = 'APP_BASKET_DELETE_HUMAN_IN_BASKET_SUCCESS'

export const APP_BASKET_BUY = 'APP_BASKET_BUY'
export const APP_BASKET_BUY_SUCCESS = 'APP_BASKET_BUY_SUCCESS'

