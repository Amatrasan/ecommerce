import * as types from './actionTypes'
const initialState = []

const stocks = (state = initialState, { type, payload }) => {
  switch (type) {
  case types.APP_STOCKS_GET_SUCCESS:
    return payload
  default:
    return state
  }
}

export default stocks
