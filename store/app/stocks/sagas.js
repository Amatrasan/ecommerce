import * as types from './actionTypes'
import { takeLatest, call } from 'redux-saga/effects'
import api from '../../../services/ApiService'

export function fetchStocksSuccess (payload) {
  return {
    type: types.APP_STOCKS_GET_SUCCESS,
    payload: payload
  }
}

export function* fetchStocks () {
  const result = yield api.getStocks()
  yield call(fetchStocksSuccess, result.data)
}



export function* stocksWatcher () {
  yield takeLatest(types.APP_STOCKS_GET, fetchStocks)
}
