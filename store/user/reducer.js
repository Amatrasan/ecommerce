import * as types from './actionTypes'
import { APP_AUTH_LOGOUT } from '../auth/actionTypes'
const initialState = null

const user = (state = initialState, { type, payload }) => {
  switch (type) {
  case types.APP_USER_GET_SUCCESS:
    return { ...payload }
  case types.APP_USER_CARD_DELETE_SUCCESS:
    return { ...state, cards: state.cards.filter(card => card.id !== payload) }
  case types.APP_USER_CARD_CREATE_SUCCESS:
    return { ...state, cards: [ payload, ...state.cards ] }
  case APP_AUTH_LOGOUT:
    return initialState
  default:
    return state
  }
}

export default user
