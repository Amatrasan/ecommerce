import * as types from './actionTypes'
import api from '../../services/ApiService'
import { put, take, takeLatest, all, call } from 'redux-saga/effects'
import { APP_AUTH_TOKEN_CLEAR } from '../auth/actionTypes'
import { APP_BASKETS_GET } from '../app/baskets/actionTypes'
import { toast } from 'react-toastify'

function* fetchUser () {
  try {
    const result = yield api.getUser()
    yield put({ type: types.APP_USER_GET_SUCCESS, payload: result.data, })
    yield put({ type: APP_BASKETS_GET })
  } catch (err) {
    yield put({ type: APP_AUTH_TOKEN_CLEAR })
  }

}

function* userCardDelete () {
  while (true) {
    const { cardId } = yield take(types.APP_USER_CARD_DELETE)
    const result = yield api.deleteCardUser()
    if (result.status >= 200) {
      yield put({ type: types.APP_USER_CARD_DELETE_SUCCESS, payload: cardId, })
      yield call(getToastMessage, 'Карта успешно удалена')
    }
  }
}

function* userCardCreate () {
  while (true) {
    const { card } = yield take(types.APP_USER_CARD_CREATE)
    const result = yield api.getUser()
    if (result.status >= 200) {
      yield put({ type: types.APP_USER_CARD_CREATE_SUCCESS, payload: { id: Date.now(), ...card }, })
      yield call(getToastMessage, 'Карта успешно добавлена')
    } else {
      yield call(getToastMessage, 'Ошибка добавления карты', 'error')
    }
  }
}

function* userUpdate () {
  while (true) {
    const { user } = yield take(types.APP_USER_UPDATE)
    const result = yield api.updateUser(user)
    yield put({ type: types.APP_USER_GET_SUCCESS, payload: result.data, })
    yield call(getToastMessage, 'Информация успешно обновлена')
  }
}

function* userGet () {
  yield takeLatest(types.APP_USER_GET, fetchUser)
}

function getToastMessage (message, type = '') {
  if (type === 'error') {
    toast.error(message || 'Что-то пошло не так')
  } else {
    toast.success(message || 'Успешно')
  }
}

export function* userWatcher () {
  yield all([
    userGet(),
    userUpdate(),
    userCardDelete(),
    userCardCreate()
  ])
}
