import { combineReducers } from 'redux'
import app from './app'
import user from './user/reducer'
import auth from './auth/reducer'
import admin from './admin'

const storeReducer = combineReducers({
  app,
  user,
  auth,
  admin
})

export default storeReducer
