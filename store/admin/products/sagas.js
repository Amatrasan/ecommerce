import api from '../../../services/ApiService'
import * as types from './actionTypes'
import { all, put, take, call } from 'redux-saga/effects'
import mockImage from './../../../public/mockups/a5.webp'
import { toast } from 'react-toastify'

function* getAdminProducts () {
  while (true) {
    const { productType } = yield take(types.ADMIN_PRODUCTS_GET)
    const result = yield api.getAdminProducts({ productType })
    yield put({ type: types.ADMIN_PRODUCTS_GET_SUCCESS, payload: { products: result.data, productType }, })
  }
}

function* createAdminProduct () {
  while (true) {
    const { product } = yield take(types.ADMIN_PRODUCTS_PRODUCT_CREATE)
    const result = yield api.createAdminProduct(product)
    yield put({ type: types.ADMIN_PRODUCTS_PRODUCT_CREATE_SUCCESS, payload: { id: Date.now() + 'a', ...product, image: mockImage }, })
    yield call(getToastMessage, 'Продукт успешно создан')
  }
}

function* updateAdminProduct () {
  while (true) {
    const { product } = yield take(types.ADMIN_PRODUCTS_PRODUCT_UPDATE)
    console.log(product, 'DA')
    const result = yield api.updateAdminProduct(product)
    console.log(result, 'DAAA')
    yield put({ type: types.ADMIN_PRODUCTS_PRODUCT_UPDATE_SUCCESS, payload: { ...product, image: mockImage }, })
    yield call(getToastMessage, 'Продукт успешно изменен')
  }
}

function getToastMessage (message, type = '') {
  if (type === 'error') {
    toast.error(message || 'Что-то пошло не так')
  } else {
    toast.success(message || 'Успешно')
  }
}

export function* adminProductsWatcher () {
  yield all([
    getAdminProducts(),
    createAdminProduct(),
    updateAdminProduct()
  ])
}
