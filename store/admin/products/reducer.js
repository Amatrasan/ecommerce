import * as types from './actionTypes'
import { APP_AUTH_LOGOUT } from '../../auth/actionTypes'

const initialState = {}

const products = (state = initialState, { type, payload })=> {
  switch (type) {
  case types.ADMIN_PRODUCTS_GET_SUCCESS:
    return { ...state, [payload.productType]: payload.products }
  case types.ADMIN_PRODUCTS_PRODUCT_CREATE_SUCCESS:
    return { ...state, [payload.category]: payload.category ? [ payload, ...state[payload.category] ] : [payload] }
  case types.ADMIN_PRODUCTS_PRODUCT_UPDATE_SUCCESS:
    return {
      ...state,
      [payload.category]: state[payload.category].map((product) => product.id === payload.id ? payload : product)
    }
  case APP_AUTH_LOGOUT:
    return initialState
  default:
    return state
  }
}

export default products
