import * as types from './actionTypes'
import { APP_AUTH_LOGOUT } from '../../auth/actionTypes'

const initialState = []

const stocks = (state = initialState, { type, payload })=> {
  switch (type) {
  case types.ADMIN_STOCKS_GET_SUCCESS:
    return payload
  case types.ADMIN_STOCKS_DELETE_SUCCESS:
    return state.filter(stock => stock.id !== payload.stockId)
  case types.ADMIN_STOCKS_CREATE_SUCCESS:
    return [ payload, ...state]
  case APP_AUTH_LOGOUT:
    return initialState
  default:
    return state
  }
}

export default stocks
