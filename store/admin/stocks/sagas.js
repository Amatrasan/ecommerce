import { all, put, take, takeLatest } from 'redux-saga/effects'
import * as types from './actionTypes'
import api from '../../../services/ApiService'

export function* fetchStocks () {
  const result = yield api.getStocks()
  yield put({ type: types.ADMIN_STOCKS_GET_SUCCESS, payload: result.data })
}

function* getAdminStocks () {
  yield takeLatest(types.ADMIN_STOCKS_GET, fetchStocks)
}

function* deleteStock () {
  while (true) {
    const { stockId } = yield take(types.ADMIN_STOCKS_DELETE)
    const result = yield api.deleteAdminStock()
    yield put({ type: types.ADMIN_STOCKS_DELETE_SUCCESS, payload: { stockId }, })
  }
}

function* createStock () {
  while (true) {
    const { stock } = yield take(types.ADMIN_STOCKS_CREATE)
    const result = yield api.createAdminStock(stock)
    yield put({ type: types.ADMIN_STOCKS_CREATE_SUCCESS, payload: { id: Date.now() + 'st', img: stock.result }, })
  }
}

export function* adminStocksWatcher () {
  yield all([
    getAdminStocks(),
    deleteStock(),
    createStock()
  ])
}
