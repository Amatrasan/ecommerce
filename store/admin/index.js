import { combineReducers } from 'redux'
import products from './products/reducer'
import stocks from './stocks/reducer'

const adminReducer = combineReducers({
  products,
  stocks
})

export default adminReducer
