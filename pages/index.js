import { useEffect } from 'react'
import PropTypes from 'prop-types'
import Header from '../components/Header'
import Footer from '../components/footer/Footer'
import GridWrapper from '../components/UI/GridWrapper'
import MainPage from '../components/MainPage'
import api from '../services/ApiService'
import { useDispatch } from 'react-redux'
import { fetchStocksSuccess } from '../store/app/stocks/sagas'
import { fetchProductsHitsSuccess } from '../store/app/products/hits/actions'
import { fetchProductsNewsSuccess } from '../store/app/products/news/actions'

const getMainData = async () => {
  const resProductsHit = await api.getProducts({ type: 'hit' })
  const resProductsNew = await api.getProducts({ type: 'news' })
  const resStocks = await api.getStocks()
  return {
    newProducts: resProductsNew.data.products,
    hitProducts: resProductsHit.data.products,
    stocks: resStocks.data
  }
}

function Home (props) {

  const dispatch = useDispatch()

  useEffect(() => {
    const fetchProductsAndStocks = async () => await getMainData()

    if (!props.newProducts || !props.hitProducts || !props.stocks) {
      fetchProductsAndStocks().then(res => {
        dispatch(fetchStocksSuccess(res.stocks))
        dispatch(fetchProductsHitsSuccess(res.hitProducts))
        dispatch(fetchProductsNewsSuccess(res.newProducts))
      })
    } else {
      dispatch(fetchStocksSuccess(props.stocks))
      dispatch(fetchProductsHitsSuccess(props.hitProducts))
      dispatch(fetchProductsNewsSuccess(props.newProducts))
    }

  }, [dispatch, props])
  return (
    <>
      <Header />
      <GridWrapper>
        <MainPage />
      </GridWrapper>
      <Footer />
    </>
  )
}

Home.propTypes = {
  newProducts: PropTypes.array,
  hitProducts: PropTypes.array,
  stocks: PropTypes.array
}

export async function getServerSideProps () {
  const resultData = await getMainData()
  return {
    props: resultData
  }
}

export default Home
