import Header from '../../components/Header'
import AdminPageComponent from '../../components/AdminPage'
import GridWrapper from '../../components/UI/GridWrapper'

const AdminPage = () => {
  return (
    <div>
      <Header admin={true} showOptions={false} />
      <GridWrapper>
        <AdminPageComponent />
      </GridWrapper>
    </div>
  )
}

export default AdminPage
