import { useRouter } from 'next/router'

const Catalog = (props) => {
  const router = useRouter()
  return (
    <div>
      <span>catalog</span>
      <button onClick={() => router.push('/')}>go Home</button>
    </div>
  )
}

export default Catalog
