import { useEffect, useState } from 'react'
import Footer from '../../../components/footer'
import Header from '../../../components/Header'
import SearchParameters from '../../../components/UI/SearchParameters'
import api from '../../../services/ApiService'
import { withTranslation } from './../../../i18n'
import PropTypes from 'prop-types'
import GridWrapper from '../../../components/UI/GridWrapper'
import ProductList from '../../../components/ProductList'
import { useRouter } from 'next/router'

const Shoes = (props) => {
  const [prod, setProd] = useState([])
  const [options, setOptions] = useState({})
  const router = useRouter()
  console.log('router', router)

  useEffect(() => {

    const fetchData = async () => {
      if (!Object.entries(props.products).length) {
        const result = await api.getProducts({ category: router.query.category })
        return { products: result.data.products, options: result.data.options }
      } else {
        return { products: props.products, options: props.options }
      }
    }
    fetchData().then(res => {
      setProd(res.products)
      setOptions(res.options)
    })
  }, [props, router.query.category])

  const handleSearch = async (val) => {
    const products = await api.getProducts({ category: 'shirts', ...val })
    setProd(products.data.products)
  }

  return (
    <>
      <Header />
      <GridWrapper>
        <SearchParameters onSearchChange={handleSearch} options={options} />
        <ProductList products={prod}/>
      </GridWrapper>
      <Footer />
    </>
  )
}

Shoes.propTypes = {
  products: PropTypes.array,
  options: PropTypes.any
}

export async function getServerSideProps () {
  const res = await api.getProducts({ category: 'shirts' })
  return {
    props: { products: res.data.products, options: res.data.options }
  }
}

export default withTranslation('shoes')(Shoes)
