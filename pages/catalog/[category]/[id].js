import { useEffect } from 'react'
import GridWrapper from '../../../components/UI/GridWrapper'
import Header from '../../../components/Header'
import Footer from '../../../components/footer'
import { useRouter } from 'next/router'
import ProductContent from '../../../components/UI/ProductContent'
import { withTranslation } from '../../../i18n'
import PropTypes from 'prop-types'
import api from './../../../services/ApiService'
import { useDispatch } from 'react-redux'

import { APP_PRODUCTS_PRODUCT_GET } from '../../../store/app/products/product/actionTypes'
import { fetchProductSuccess } from '../../../store/app/products/product/sagas'


const ShirtsProduct = (props) => {
  const router = useRouter()
  const dispatch = useDispatch()

  useEffect(() => {
    if (!props.product && router.query && router.query.id)  {
      dispatch({ type: APP_PRODUCTS_PRODUCT_GET, id: router.query.id })
    } else {
      dispatch(fetchProductSuccess(props.product))
    }
  }, [dispatch, props, router])

  return (
    <>
      <Header />
      <GridWrapper>
        <ProductContent />
      </GridWrapper>
      <Footer />
    </>
  )
}

ShirtsProduct.propTypes = {
  product: PropTypes.object
}

export async function getServerSideProps (ctx) {
  const res = await api.getProduct(ctx.query.id)
  const product = res.data
  return {
    props: { product: product }
  }
}

export default withTranslation('productContent')(ShirtsProduct)
