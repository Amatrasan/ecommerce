import Link from 'next/link'

const NotFoundPage = () => {
  return (
    <div>
      <h3>Нет такой страницы</h3>
      <Link href={'/'}>На главную</Link>
    </div>
  )
}

export default NotFoundPage
