import './globals.scss'
import { appWithTranslation } from '../i18n'
import App from 'next/app'
import { Provider, useStore } from 'react-redux'
import PropTypes from 'prop-types'
import { PersistGate } from 'redux-persist/integration/react'
import wrapper from './../services/StoreService'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

function MyApp ({ Component, pageProps }) {
  const store = useStore()
  return (
    <Provider store={store}>
      <PersistGate persistor={store.__PERSISTOR} loading={null}>
        <ToastContainer />
        <Component {...pageProps} />
      </PersistGate>
    </Provider>
  )
}

MyApp.propTypes = {
  Component: PropTypes.func.isRequired,
  pageProps: PropTypes.object.isRequired
}

MyApp.getInitialProps = async (appContext) => {
  const appProps = await App.getInitialProps(appContext)
  const defaultProps = appContext.Component.defaultProps
  return {
    ...appProps,
    pageProps: {
      namespacesRequired: [...(appProps.pageProps.namespacesRequired || []), ...(defaultProps?.i18nNamespaces || [])]
    }
  }
}

export default wrapper.withRedux(appWithTranslation(MyApp))



