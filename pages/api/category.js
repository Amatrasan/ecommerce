import getRandomId from '../../utils/getRandomId'

export default async (req, res) => {
  // const mockup = mockupData()
  // res.statusCode = 200
  // console.log('mockup',mockup)
  // res.json(mockup)

  if (req.method === 'GET') {
    const mockup = mockupData()
    res.statusCode = 200
    console.log('mockup',mockup)
    res.json(mockup)
  } else if (req.method === 'POST') {
    const cat = {
      id: '123' + getRandomId('3'),
      label: req.body.name,
      code: getRandomId('5')
    }
    globalThis.CATEGORIES = [...globalThis.CATEGORIES,cat]
    res.statusCode = 200
    res.json(cat)
  } else {
    const mockup = mockupData()
    res.statusCode = 200
    console.log('mockup',mockup)
    res.json(mockup)
  }

}

const mockupData = () => {
  if (globalThis.CATEGORIES) {
    console.log('CATEGORIES', globalThis.CATEGORIES)
    return globalThis.CATEGORIES
  } else {
    const res = [
      {
        id: '123',
        label: 'Рубашки',
        code: 'shirts'
      },
      {
        id: '1234',
        label: 'Обувь',
        code: 'shoes'
      },
      {
        id: '1235',
        label: 'Аксессуары',
        code: 'accessories'
      },
      {
        id: '1236',
        label: 'Категория 3',
        code: getRandomId(5)
      }
    ]
    globalThis.CATEGORIES = res
    return res
  }
}
