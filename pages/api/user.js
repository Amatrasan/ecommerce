export default async (req, res) => {

  const mockup = {
    id: '12da',
    role: 'user',
    lastName: '',
    firstName: '',
    patronymic: '',
    email: 'erold3+3@yandex.ru',
    phone: '89185913884',
    country: 'Russia',
    city: 'RnD',
    address: 'Lenina street, 152',
    cards: [
      {
        id: 1,
        number: 1234567887654321,
        date: '12/22'
      },
      {
        id: 2,
        number: 9934567887654321,
        date: '12/22'
      }
    ]
  }
  if (req.method === 'GET') {
    res.statusCode = 200
    res.json(mockup)
  } else {
    res.statusCode = 200
    res.json(req.body.user)
  }

}
