export default async (req, res) => {
  console.log('globalThis', globalThis)
  // globalThis.TEMP_SUCCESS = 'SUCCESS'
  res.statusCode = 200
  res.json({ user: req.body, token: getToken(3) })
}

const getToken = (minutes = 1) => {
  return {
    accessToken: 'access_token_1234',
    refreshToken: 'refresh_token_12345',
    expired_at: Date.now() + (minutes * 60 * 1000)
  }
}

