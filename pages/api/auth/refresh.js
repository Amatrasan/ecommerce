export default async (req, res) => {
  console.log(req.body)
  if (req.body.token) {
    const token = getToken(1)
    res.statusCode = 200
    res.json({ ...token })
  } else {
    res.statusCode = 401
    res.json({ message: 'Токен просрочен' })
  }

}

const getToken = (minutes = 1) => {
  return {
    accessToken: 'access_token_1234',
    refreshToken: 'refresh_token_12345',
    expired_at: Date.now() + (minutes * 60 * 1000)
  }
}
