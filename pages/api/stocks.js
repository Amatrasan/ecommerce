import mockupStock from './../../public/mockups/mockupStock.png'

export default async (req, res) => {
  const mockup = mockupData()
  res.statusCode = 200
  res.json(mockup)
}

const mockupData = () => {
  let array = []
  for (let i = 0; i < 30; i++) {
    array.push({ id: i, img: mockupStock })
  }
  return array
}
