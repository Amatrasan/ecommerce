import globalImage from './../../../public/mockups/a1.webp'

export default async (req, res) => {
  console.log('IN REQUEST', req.query)
  const mockup = await mockupData(req.query.id)
  res.statusCode = 200
  res.json(mockup)
}

const mockupData = async (productId) => {
  const randomInt = getRandomInt(1, 10000)
  return {
    id: productId + '',
    category: randomInt % 2 ? 'shoes' : randomInt % 3 ? 'shirts' : 'accessories',
    name: 'Какой либо продукт',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    price: randomInt,
    priceOld: randomInt % 2 ? 0 : randomInt * 12 + 400,
    image: globalImage,
    colors: ['red', 'blue', 'white', 'black'],
    sizes: ['X', 'S', 'M'],
    count: 12,
    isMyProduct: !!(randomInt % 2)
  }
}

const getRandomInt = (min, max) => {
  return Math.floor(Math.random() * (max - min) + min)
}
