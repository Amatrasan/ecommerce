import globalImage from './../../../public/mockups/a1.webp'
import globalImage2 from './../../../public/mockups/t3.webp'
import globalImage3 from './../../../public/mockups/a3.webp'

export default async (req, res) => {
  const mockup = await mockupData(req.query)
  const optionParams = {
    colors: ['red', 'black', 'white'],
    price: { min: 1100, max: 340000 },
    sizes: ['X', 'S', 'XL']
  }
  res.statusCode = 200
  if (req.query.admin) {
    res.json(mockup)
  } else {
    res.json({ products: mockup, options: optionParams })
  }

}

const mockupData = async ({ size, price = { min: 10, max: 100 }, color, sort, offset = 0, productType }) => {
  let products = []
  let priceMinMax = typeof price === 'string' ? JSON.parse(price) : price
  for (let i = offset; i < 100; i++) {
    products.push({
      id: i + 'p',
      category: getCategory(productType),
      name: ('Случайный продукт ' + i),
      price: getRandomInt(priceMinMax.min, priceMinMax.max + 1),
      priceOld: i % 2 ? 0 : i * 12 + 400,
      image: i % 2 ? globalImage : i % 3 ? globalImage2 : globalImage3,
      colors: color ? color[getRandomInt(0, color.length)] : i % 2 ? ['red'] : i % 3 ? ['blue'] : ['white', 'red'],
      size: size ? size[getRandomInt(0, size.length)] : i % 2 ? 'S' : i % 3 ? 'XS' : 'M',
      count: 12,
    })
  }
  if (sort) {
    const compareUp = (a, b) => {
      return a.price - b.price
    }

    const compareDown = (a, b) => {
      return b.price - a.price
    }

    if (sort === 'Цена по убыванию') {
      products.sort(compareDown)
    } else {
      products.sort(compareUp)
    }
  }
  return products
}

const getCategory = (category) => {
  if (category) {
    return category
  } else {
    return getRandomInt(1, 100) % 2 ? 'shoes' : getRandomInt(1, 100) % 3 ? 'shirts' : 'accessories'
  }
}

const getRandomInt = (min, max) => {
  return Math.floor(Math.random() * (max - min) + min)
}
