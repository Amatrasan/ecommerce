import { useRouter } from 'next/router'
import Footer from '../../components/footer'
import Header from '../../components/Header'
import GridWrapper from '../../components/UI/GridWrapper'
import { useSelector } from 'react-redux'
import CabinetComponent from '../../components/CabinetComponent'

const Cabinet = (props) => {
  const auth = useSelector(state => state.auth?.token)
  const router = useRouter()

  if (auth) {
    return (
      <>
        <Header showOptions={false} />
        <GridWrapper>
          <CabinetComponent />
        </GridWrapper>
        <Footer />
      </>
    )
  } else {
    router.push('/')
    return null
  }

}

Cabinet.defaultProps = {
  i18nNamespaces: ['cabinet']
}

export default Cabinet
